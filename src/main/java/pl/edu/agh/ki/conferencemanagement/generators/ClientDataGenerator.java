package pl.edu.agh.ki.conferencemanagement.generators;

import pl.edu.agh.ki.conferencemanagement.domain.Client;
import pl.edu.agh.ki.conferencemanagement.utils.PeopleUtils;

import java.sql.Timestamp;

/**
 * @author Michał Ciołczyk
 */
public class ClientDataGenerator extends AbstractDataGenerator<Client> {

    private final PeopleUtils peopleUtils;

    private static final String companies[] = {"ABB", "Sun", "Oracle", "IBM", "Microsoft", "Sabre",
            "MPK Kraków", "Motorola", "Tramwaje Śląskie", "PWN", "Znak", "Iskry", "Amber",
            "Troll Company"};

    public ClientDataGenerator() {
        super();
        peopleUtils = PeopleUtils.getInstance();
    }

    @Override
    public Client generate() {
        Client client = new Client();

        int type = random.nextInt(2);

        String firstName = peopleUtils.getFirstName();
        String lastName = peopleUtils.getLastName();
        String address = peopleUtils.getStreet() + " " + random.nextInt(256);
        String city = peopleUtils.getCity();
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setType((type>0) ? "c" : "p");
        client.setAddress(address);
        client.setCity(city);
        client.setCountry("Polska");

        if (type > 0) {
            String companyName = companies[random.nextInt(companies.length)];
            client.setCompanyName(companyName);
            client.setNip(Long.valueOf(1004230000L+random.nextInt(89999)).toString());
            client.setRegon(Long.valueOf(10042300000000L + 1111 * random.nextInt(89999)).toString());
        }

        client.setPhone(Long.valueOf(100000000+random.nextInt(899999999)).toString());
        String registrationDate = Integer.valueOf(2010+random.nextInt(2)).toString()+"-";
        int month = random.nextInt(12)+1;
        registrationDate = registrationDate + ((month<10)?"0"+month:month) +"-";
        int day = random.nextInt(12)+1;
        registrationDate = registrationDate + ((day<10)?"0"+day:day)+" 1"+random.nextInt(9);
        registrationDate = registrationDate + ":01:03";
        client.setRegistrationDate(Timestamp.valueOf(registrationDate));
        String username = firstName.substring(0,3)+lastName.substring(0,3)+Long.valueOf(random.nextInt(8999999)+1000000000L).toString();
        client.setUsername(username);
        client.setPassword(Long.valueOf(Math.abs((long) username.hashCode() + random.nextInt(1000000000) + 1000000)).toString());

        return client;
    }
}
