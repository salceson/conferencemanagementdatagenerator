package pl.edu.agh.ki.conferencemanagement.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:31
 */
public class ClientPayment implements Entity {
    private Integer id;
    private Integer reservationId;
    private Integer clientId;
    private Integer discountId;
    private Timestamp paymentDateTo;
    private Timestamp paymentPaidDate;
    private BigDecimal paymentAmount;
    private BigDecimal paymentPaidAmount;

    public Integer getId() {
        return id;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public Timestamp getPaymentDateTo() {
        return paymentDateTo;
    }

    public void setPaymentDateTo(Timestamp paymentDateTo) {
        this.paymentDateTo = paymentDateTo;
    }

    public Timestamp getPaymentPaidDate() {
        return paymentPaidDate;
    }

    public void setPaymentPaidDate(Timestamp paymentPaidDate) {
        this.paymentPaidDate = paymentPaidDate;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public BigDecimal getPaymentPaidAmount() {
        return paymentPaidAmount;
    }

    public void setPaymentPaidAmount(BigDecimal paymentPaidAmount) {
        this.paymentPaidAmount = paymentPaidAmount;
    }

    @Override
    public Map<String,Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"ReservationID\"", reservationId);
        objects.put("\"ClientID\"", clientId);
        objects.put("\"DiscountID\"", discountId);
        objects.put("\"PaymentDateTo\"", paymentDateTo);
        objects.put("\"PaymentPaidDate\"", paymentPaidDate);
        objects.put("\"PaymentAmount\"", paymentAmount);
        objects.put("\"PaymentPaidAmount\"", paymentPaidAmount);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ClientPayments\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "PaymentID";
    }
}
