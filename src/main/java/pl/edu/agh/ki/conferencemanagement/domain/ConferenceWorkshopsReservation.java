package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:12
 */
public class ConferenceWorkshopsReservation implements Entity {
    private Integer workshopId;
    private Integer conferenceDayId;
    private Integer reservationId;
    private Integer numOfPeople;
    private Integer numOfStudents;

    public Integer getWorkshopId() {
        return workshopId;
    }

    public void setWorkshopId(Integer workshopId) {
        this.workshopId = workshopId;
    }

    public Integer getConferenceDayId() {
        return conferenceDayId;
    }

    public void setConferenceDayId(Integer conferenceDayId) {
        this.conferenceDayId = conferenceDayId;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(Integer numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public Integer getNumOfStudents() {
        return numOfStudents;
    }

    public void setNumOfStudents(Integer numOfStudents) {
        this.numOfStudents = numOfStudents;
    }

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"WorkshopID\"", workshopId);
        objects.put("\"ConferenceDayID\"", conferenceDayId);
        objects.put("\"ReservationID\"", reservationId);
        objects.put("\"NumOfPeople\"", numOfPeople);
        objects.put("\"NumOfStudents\"", numOfStudents);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ConferenceWorkshopsReservations\"";
    }

    @Override
    public void setID(Integer id) {

    }

    @Override
    public String getIDColumnName() {
        return null;
    }
}
