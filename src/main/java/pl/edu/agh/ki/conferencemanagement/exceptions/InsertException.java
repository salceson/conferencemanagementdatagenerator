package pl.edu.agh.ki.conferencemanagement.exceptions;

/**
 * @author Michał Ciołczyk
 */
public class InsertException extends RuntimeException {
    public InsertException() {
        super("Insert operation has not been completed");
    }
}
