package pl.edu.agh.ki.conferencemanagement.utils;

import pl.edu.agh.ki.conferencemanagement.domain.Entity;

import javax.sql.DataSource;

/**
 * @author Michał Ciołczyk
 */
public interface JDBCInsertWrapper {
    void setupConnection(DataSource dataSource, String schemaName);
    void insert(Entity entity);
}
