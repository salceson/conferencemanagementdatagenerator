package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:21
 */
public class ConferenceParticipant implements Entity {
    private Integer id;
    private Integer clientId;
    private String participantFirstName;
    private String participantLastName;
    private String studentIDCard;
    private Integer reservationId;

    public Integer getId() {
        return id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getParticipantFirstName() {
        return participantFirstName;
    }

    public void setParticipantFirstName(String participantFirstName) {
        this.participantFirstName = participantFirstName;
    }

    public String getParticipantLastName() {
        return participantLastName;
    }

    public void setParticipantLastName(String participantLastName) {
        this.participantLastName = participantLastName;
    }

    public String getStudentIDCard() {
        return studentIDCard;
    }

    public void setStudentIDCard(String studentIDCard) {
        this.studentIDCard = studentIDCard;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("\"ClientID\"", clientId);
        map.put("\"ParticipantFirstName\"", participantFirstName);
        map.put("\"ParticipantLastName\"", participantLastName);
        map.put("\"StudentIDCard\"", studentIDCard);
        map.put("\"ReservationID\"", reservationId);

        return map;
    }

    @Override
    public String getTableName() {
        return "\"ConferenceParticipants\"";
    }

    @Override
    public void setID(Integer id) {
        this.id = id;
    }

    @Override
    public String getIDColumnName() {
        return "ParticipantID";
    }
}
