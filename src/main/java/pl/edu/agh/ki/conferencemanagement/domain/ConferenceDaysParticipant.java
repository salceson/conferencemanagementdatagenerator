package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:28
 *
 *
 */

public class ConferenceDaysParticipant implements Entity {
    private Integer participantId;
    private Integer conferenceDayId;
    private Integer reservationId;

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public Integer getConferenceDayId() {
        return conferenceDayId;
    }

    public void setConferenceDayId(Integer conferenceDayId) {
        this.conferenceDayId = conferenceDayId;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("\"ParticipantID\"", participantId);
        map.put("\"ConferenceDayID\"", conferenceDayId);
        map.put("\"ReservationID\"", reservationId);

        return map;
    }

    @Override
    public String getTableName() {
        return "\"ConferenceDaysParticipants\"";
    }

    @Override
    public void setID(Integer id) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getIDColumnName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
