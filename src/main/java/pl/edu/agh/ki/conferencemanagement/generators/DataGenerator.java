package pl.edu.agh.ki.conferencemanagement.generators;

/**
 * @author Michał Ciołczyk
 */
public interface DataGenerator<T> {
    T generate();
}
