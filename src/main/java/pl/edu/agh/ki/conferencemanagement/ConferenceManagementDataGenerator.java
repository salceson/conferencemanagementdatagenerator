package pl.edu.agh.ki.conferencemanagement;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import pl.edu.agh.ki.conferencemanagement.domain.*;
import pl.edu.agh.ki.conferencemanagement.generators.*;
import pl.edu.agh.ki.conferencemanagement.utils.*;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceManagementDataGenerator {
    private final Set<Conference> conferences;
    private final Set<Client> clients;
    private final Set<ConferenceDay> conferenceDays;
    private final JDBCInsertWrapper wrapper;
    private final JDBCInsertWrapper wrapperWithGeneratedIDs;
    private final SimpleJdbcTemplate template;
    private final Random random;
    private final Set<ConferenceWorkshop> workshops;
    private final Map<Integer, ConferencePaymentsDiscount> discounts;

    public ConferenceManagementDataGenerator(DataSource source, String schema) {
        wrapper = new JDBCInsertWrapperImpl();
        wrapperWithGeneratedIDs = new JDBCInsertWrapperWithGeneratedIDs();
        template = new SimpleJdbcTemplate(source);

        wrapper.setupConnection(source, schema);
        wrapperWithGeneratedIDs.setupConnection(source, schema);

        conferences = new HashSet<Conference>();
        clients = new HashSet<Client>();
        conferenceDays = new HashSet<ConferenceDay>();
        workshops = new HashSet<ConferenceWorkshop>();
        discounts = new HashMap<Integer, ConferencePaymentsDiscount>();

        random = new Random();

        generateConferences();
        generateClients();
        generateReservations();
        //template.update(
                //"DELETE FROM \"ConferenceManagement\".\"Clients\" WHERE \"ClientID\" NOT IN (SELECT ClientID FROM \"ConferenceManagement\".\"ClientConferenceReservations\")",
                //null);
    }

    private void generateReservations() {
        Iterator<Client> clientIterator = clients.iterator();
        ConferenceParticipantGenerator generator = new ConferenceParticipantGenerator();

        //Iterates for all conferences
        for (final Conference conference : conferences) {
            int inConference = 0;
            //until we have the least minimum amount
            while (inConference < 195) {

                //We get the client
                Client client = clientIterator.next();

                //Make a reservation for him for the conference
                ConferenceReservation reservation = new ConferenceReservation();

                BigDecimal totalFee = new BigDecimal(0);

                reservation.setClientId(client.getId());
                reservation.setConferenceId(conference.getId());

                String conferenceStartDate = conference.getStartDate().toString();
                int conferenceStartDay = Integer.parseInt(conferenceStartDate.substring(8, 10));
                int resDay = conferenceStartDay - (random.nextInt(2) > 0 ? 3 : 7);

                String reservationDate = conferenceStartDate.substring(0, 8) + (resDay < 10 ? "0" : "") + resDay;
                reservation.setReservationDate(Timestamp.valueOf(reservationDate + " 12:32:23"));

                wrapperWithGeneratedIDs.insert(reservation);

                generator.setClient(client);
                generator.setReservation(reservation);

                //Create participants set
                Set<ConferenceParticipant> participants = new HashSet<ConferenceParticipant>();

                //We get the conference days for the conference
                Set<ConferenceDay> days = SetUtils.filter(conferenceDays, new Predicate<ConferenceDay>() {
                    @Override
                    public boolean apply(ConferenceDay object) {
                        return object.getId() == conference.getId();
                    }
                });

                //We create participants
                for (int i = 0; i < (10 / days.size() * days.size()); i++) {
                    ConferenceParticipant participant = generator.generate();
                    participants.add(participant);
                    wrapperWithGeneratedIDs.insert(participant);
                    inConference++;
                }

                Iterator<ConferenceParticipant> participantIterator = participants.iterator();

                //We iterate for conference days
                for (final ConferenceDay day : days) {
                    ConferenceDaysReservation daysReservation = new ConferenceDaysReservation();
                    daysReservation.setConferenceDayId(day.getId());
                    daysReservation.setReservationId(reservation.getId());

                    //Gets the workshos for that day
                    Set<ConferenceWorkshop> workshopsInDay = SetUtils.filter(workshops, new Predicate<ConferenceWorkshop>() {
                        @Override
                        public boolean apply(ConferenceWorkshop object) {
                            return object.getConferenceDayId() == day.getId();
                        }
                    });

                    Set<WorkshopParticipant> workshopParticipants = new HashSet<WorkshopParticipant>();
                    Set<ConferenceWorkshopsReservation> workshopsReservations = new HashSet<ConferenceWorkshopsReservation>();
                    Set<ConferenceDaysParticipant> daysParticipants = new HashSet<ConferenceDaysParticipant>();

                    int numOfPeople = 0;

                    //We add participants to that day and one workshop
                    for (int i = 0; i < (10 / days.size()); i++) {
                        ConferenceDaysParticipant conferenceDaysParticipant = new ConferenceDaysParticipant();

                        conferenceDaysParticipant.setReservationId(reservation.getId());
                        conferenceDaysParticipant.setConferenceDayId(day.getId());

                        ConferenceParticipant participant = participantIterator.next();
                        conferenceDaysParticipant.setParticipantId(participant.getId());

                        daysParticipants.add(conferenceDaysParticipant);

                        totalFee = totalFee.add(day.getFee().multiply((participant.getStudentIDCard() == null) ? BigDecimal.ONE :
                                BigDecimal.valueOf(day.getStudentDiscount())));

                        if (participant.getStudentIDCard() == null) {
                            numOfPeople++;
                        }

                        Iterator<ConferenceWorkshop> workshopIterator = workshopsInDay.iterator();
                        ConferenceWorkshop workshop = null;
                        boolean notToInsert = false;
                        for (int j = day.getDay() * 4 - 4; j < i * day.getDay()
                                && workshopIterator.hasNext(); j++) {
                            workshop = workshopIterator.next();
                        }

                        for (ConferenceWorkshopsReservation workshopsReservation : workshopsReservations) {
                            if(workshop==null || (
                                    workshopsReservation.getConferenceDayId()==day.getId() &&
                                    workshopsReservation.getWorkshopId()==workshop.getId() &&
                                    workshopsReservation.getReservationId()==reservation.getId())){
                                notToInsert = true;
                                break;
                            }
                        }
                        if(notToInsert || workshop==null) continue;
                        ConferenceWorkshopsReservation workshopsReservation = new ConferenceWorkshopsReservation();
                        workshopsReservation.setReservationId(reservation.getId());
                        workshopsReservation.setConferenceDayId(day.getId());
                        workshopsReservation.setNumOfStudents(participant.getStudentIDCard() == null ? 1 : 0);
                        workshopsReservation.setNumOfPeople(1 - workshopsReservation.getNumOfStudents());
                        workshopsReservation.setWorkshopId(workshop.getId());
                        workshopsReservations.add(workshopsReservation);
                        totalFee = totalFee.add(workshop.getWorkshopFee().multiply(
                                (participant.getStudentIDCard() == null) ? BigDecimal.ONE : BigDecimal.valueOf(
                                        workshop.getWorkshopStudentDiscount()
                                )
                        ));

                        WorkshopParticipant workshopParticipant = new WorkshopParticipant();
                        workshopParticipant.setConferenceDayID(day.getId());
                        workshopParticipant.setParticipantID(participant.getId());
                        workshopParticipant.setReservationID(reservation.getId());
                        workshopParticipant.setWorkshopID(workshop.getId());
                        workshopParticipants.add(workshopParticipant);

                    }

                    daysReservation.setNumOfPeople(numOfPeople);
                    daysReservation.setNumOfStudents(10 / days.size() - numOfPeople);
                    wrapper.insert(daysReservation);

                    for (ConferenceDaysParticipant daysParticipant : daysParticipants) {
                        wrapper.insert(daysParticipant);
                    }

                    for (ConferenceWorkshopsReservation workshopsReservation : workshopsReservations) {
                        wrapper.insert(workshopsReservation);
                    }

                    for (WorkshopParticipant workshopParticipant : workshopParticipants) {
                        wrapper.insert(workshopParticipant);
                    }
                }

                ConferencePaymentsDiscount discount = discounts.get(conference.getId());
                totalFee = totalFee.multiply(BigDecimal.valueOf((discount != null) ? discount.getDiscount() : 1.0));
                ClientPayment clientPayment = new ClientPayment();
                clientPayment.setReservationId(reservation.getId());
                clientPayment.setClientId(client.getId());
                clientPayment.setDiscountId(discount.getId());
                clientPayment.setPaymentDateTo(Timestamp.valueOf(conference.getStartDate().toString() + " 00:00:00"));
                clientPayment.setPaymentPaidDate(clientPayment.getPaymentDateTo());
                clientPayment.setPaymentAmount(totalFee);
                clientPayment.setPaymentPaidAmount(totalFee);
                wrapperWithGeneratedIDs.insert(clientPayment);

            }
        }
    }

    private void generateClients() {
        ClientDataGenerator clientDataGenerator = new ClientDataGenerator();

        for (int i = 0; i < 1500; i++) {
            Client client = clientDataGenerator.generate();
            clients.add(client);
            wrapperWithGeneratedIDs.insert(client);
        }
    }

    private void generateConferences() {
        ConferenceDataGenerator conferenceDataGenerator = new ConferenceDataGenerator();

        for (int year = 2011; year <= 2013; year++) {
            for (int month = 1; month <= 12; month++) {
                String dateStart = year + "-" + ((month < 10) ? "0" : "") + month + "-";
                Date startDate = Date.valueOf(dateStart + "08");
                conferenceDataGenerator.setStartDate(startDate);
                conferences.add(conferenceDataGenerator.generate());
                startDate = Date.valueOf(dateStart + "23");
                conferenceDataGenerator.setStartDate(startDate);
                conferences.add(conferenceDataGenerator.generate());
            }
        }

        for (Conference conference : conferences) {
            wrapperWithGeneratedIDs.insert(conference);
            Date startDate = conference.getStartDate();
            Date endDate = conference.getEndDate();
            int startDay = Integer.parseInt(startDate.toString().substring(8, 10));
            int endDay = Integer.parseInt(endDate.toString().substring(8, 10));
            ConferencePaymentsDiscount discount = new ConferencePaymentsDiscount();
            discount.setConferenceId(conference.getId());
            discount.setDiscount(1 - random.nextDouble() * 0.3);
            int discountDay = startDay - 7;
            discount.setUntilDate(Timestamp.valueOf(startDate.toString().substring(0, 8)
                    + (discountDay < 10 ? "0" : "") + discountDay + " 00:00:00"));
            wrapperWithGeneratedIDs.insert(discount);
            discounts.put(conference.getId(), discount);
            generateConferenceDaysForConference(conference, startDay, endDay);
        }
    }

    private void generateConferenceDaysForConference(Conference conference, int startDay, int endDay) {
        ConferenceDayDataGenerator generator = new ConferenceDayDataGenerator();
        generator.setConference(conference);
        int days = endDay - startDay + 1;
        for (int i = 0; i < days; i++) {
            generator.setDay(i);
            ConferenceDay day = generator.generate();
            conferenceDays.add(day);
            wrapperWithGeneratedIDs.insert(day);
            generateConferenceWorkshopsForDay(conference, day);
        }
    }

    private void generateConferenceWorkshopsForDay(Conference conference, ConferenceDay day) {
        int hour = 8;

        ConferenceWorkshopGenerator generator = new ConferenceWorkshopGenerator();

        generator.setConference(conference);
        generator.setConferenceDay(day);

        for (int i = 0; i < 4; i++) {
            generator.setStartHour(hour);

            ConferenceWorkshop workshop = generator.generate();
            workshops.add(workshop);
            wrapperWithGeneratedIDs.insert(workshop);

            if (hour == 12) {
                hour += 2;
            }

            hour += 2;
        }
    }

    public static void main(String[] args) {
        DataSource source = new DriverManagerDataSource("org.postgresql.Driver",
                "jdbc:postgresql://localhost:5432/conferences", "user", "password");

        new ConferenceManagementDataGenerator(source, "\"ConferenceManagement\"");

    }
}
