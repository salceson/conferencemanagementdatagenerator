package pl.edu.agh.ki.conferencemanagement.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:36
 */
public class ConferencePaymentsDiscount implements Entity {
    private Integer id;
    private Integer conferenceId;
    private Timestamp untilDate;
    private double discount;

    public Integer getId() {
        return id;
    }

    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }

    public Timestamp getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(Timestamp untilDate) {
        this.untilDate = untilDate;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }


    @Override
    public Map<String,Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"ConferenceID\"", conferenceId);
        objects.put("\"UntilDate\"", untilDate);
        objects.put("\"Discount\"", discount);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ConferencePaymentsDiscounts\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "DiscountID";
    }
}
