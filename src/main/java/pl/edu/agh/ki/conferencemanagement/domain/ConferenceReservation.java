package pl.edu.agh.ki.conferencemanagement.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 00:59
 */
public class ConferenceReservation implements Entity {
    private Integer id;
    private Integer clientId;
    private Integer conferenceId;
    private Timestamp reservationDate;

    public Integer getId() {
        return id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }

    public Timestamp getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Timestamp reservationDate) {
        this.reservationDate = reservationDate;
    }

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        final Map<String, Object> objects = new HashMap<String, Object>();

        objects.put("\"ClientID\"", clientId);
        objects.put("\"ConferenceID\"", conferenceId);
        objects.put("\"ReservationDate\"", reservationDate);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ClientConferenceReservations\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "ReservationID";
    }
}
