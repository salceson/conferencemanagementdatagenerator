--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.11
-- Dumped by pg_dump version 9.1.11
-- Started on 2014-01-21 12:17:16 CET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 21847)
-- Name: ConferenceManagement; Type: SCHEMA; Schema: -; Owner: salceson
--

CREATE SCHEMA "ConferenceManagement";


ALTER SCHEMA "ConferenceManagement" OWNER TO salceson;

--
-- TOC entry 192 (class 3079 OID 11683)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 192
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = "ConferenceManagement", pg_catalog;

--
-- TOC entry 215 (class 1255 OID 30565)
-- Dependencies: 601 7
-- Name: calculatepayment(integer); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION calculatepayment(reservationid integer) RETURNS money
    LANGUAGE plpgsql
    AS $_$
DECLARE
	rec RECORD;
	discount double precision;
	total money;
BEGIN
	total := 0;
	EXECUTE 'SELECT d."Discount"
	FROM "ConferenceManagement"."ConferencePaymentsDiscounts" d
	JOIN "ConferenceManagement"."ClientConferenceReservations" r
	ON d."ConferenceID"=r."ConferenceID"
	WHERE r."ReservationID"=($1) AND d."UntilDate" <= current_timestamp
	ORDER BY d."Discount" DESC LIMIT 1
	' INTO discount USING reservationid;
	IF discount IS NULL THEN discount := 1;
	END IF;
	FOR rec IN
		SELECT (discount*d."ConferenceDayFee"*(rd."NumOfPeople"+rd."NumOfStudents"*d."ConferenceDayStudentDiscount"))::money AS amount
		FROM "ConferenceManagement"."ConferenceDays" d
		JOIN "ConferenceManagement"."ClientConferenceDaysReservations" rd
		ON d."ConferenceDayID"=rd."ConferenceDayID"
		WHERE rd."ReservationID"=reservationid
	LOOP
		total := total + rec.amount;
	END LOOP;
	FOR rec IN
		SELECT (discount*w."WorkshopFee"*(rw."NumOfPeople"+rw."NumOfStudents"*w."WorkshopStudentDiscount"))::money AS amount
		FROM "ConferenceManagement"."ConferenceWorkshops" w
		JOIN "ConferenceManagement"."ConferenceWorkshopsReservations" rw
		ON w."WorkshopID"=rw."WorkshopID"
		WHERE rw."ReservationID"=reservationid
	LOOP
		total := total + rec.amount;
	END LOOP;
	RETURN total;
END
$_$;


ALTER FUNCTION "ConferenceManagement".calculatepayment(reservationid integer) OWNER TO salceson;

--
-- TOC entry 211 (class 1255 OID 30559)
-- Dependencies: 7 601
-- Name: conference_days_participants_inserting_trigger(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION conference_days_participants_inserting_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	maxsumofpeople integer;
	maxsumofstudents integer;
	sumofpeople integer;
	sumofstudents integer;
	isstudent boolean;
BEGIN
	EXECUTE 'SELECT "NumOfPeople" FROM "ConferenceManagement"."ClientConferenceDaysReservations" 
	WHERE "ReservationID"=($1) AND "ConferenceDayID"=($2)'
	INTO maxsumofpeople USING NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT "NumOfStudents" FROM "ConferenceManagement"."ClientConferenceDaysReservations" 
	WHERE "ReservationID"=($1) AND "ConferenceDayID"=($2)'
	INTO maxsumofstudents USING NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT COUNT(*) FROM "ConferenceManagement"."ConferenceDaysParticipants" dp
	JOIN "ConferenceManagement"."ConferenceParticipants" p ON
	dp."ParticipantID"=p."ParticipantID"
	WHERE dp."ReservationID"=($1) AND dp."ConferenceDayID"=($2)
	AND p."StudentIDCard" IS NULL' INTO sumofpeople USING
	NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT COUNT(*) FROM "ConferenceManagement"."ConferenceDaysParticipants" dp
	JOIN "ConferenceManagement"."ConferenceParticipants" p ON
	dp."ParticipantID"=p."ParticipantID"
	WHERE dp."ReservationID"=($1) AND dp."ConferenceDayID"=($2)
	AND p."StudentIDCard" IS NOT NULL' INTO sumofstudents USING
	NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT ("StudentIDCard" IS NOT NULL)
	FROM "ConferenceManagement"."ConferenceParticipants" WHERE "ParticipantID"=($1)'
	INTO isstudent USING NEW."ParticipantID";
	IF isstudent THEN sumofstudents := sumofstudents + 1;
	ELSE sumofpeople := sumofpeople + 1;
	END IF;
	IF sumofpeople > maxsumofpeople OR sumofstudents > maxsumofstudents THEN
		RAISE EXCEPTION 'Too much people/students';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".conference_days_participants_inserting_trigger() OWNER TO salceson;

--
-- TOC entry 208 (class 1255 OID 30561)
-- Dependencies: 7 601
-- Name: conference_participants_update_trigger(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION conference_participants_update_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	rec RECORD;
	colname TEXT;
BEGIN
	IF NEW."StudentIDCard" IS NOT NULL THEN colname := '"NumOfStudents"';
	ELSE colname := '"NumOfPeople"';
	END IF;
	FOR rec IN
		SELECT COUNT(dp."ParticipantID") AS mycount, dp.colname AS maxcount, dp."ResrvationID"
		FROM "ConferenceManagement"."ConferenceDaysParticipants" dp
		JOIN "ConferenceManagement"."ConferenceDaysReservations" dr
		ON dp."ConferenceDayID"=dr."ConferenceDayID" AND
		dp."ReservationID"=dr."ReservationID"
		WHERE dp."ParticipantID"=NEW."ParticipantID"
		GROUP BY dp."ReservationID", maxcount
	LOOP
		IF rec.mycount > rec.maxcount THEN
			RAISE EXCEPTION 'Too much students/people';
		END IF;
	END LOOP;
	FOR rec IN
		SELECT COUNT(wp."ParticipantID") AS mycount, wp.colname AS maxcount, wp."ResrvationID"
		FROM "ConferenceManagement"."ConferenceDaysParticipants" wp
		JOIN "ConferenceManagement"."ConferenceDaysReservations" wr
		ON wp."ConferenceDayID"=we."ConferenceDayID" AND
		wp."ReservationID"=wr."ReservationID" AND
		wp."WorkshopID"=wr."WorkshopID"
		WHERE wp."ParticipantID"=NEW."ParticipantID"
		GROUP BY wp."ReservationID", maxcount
	LOOP
		IF rec.mycount > rec.maxcount THEN
			RAISE EXCEPTION 'Too much students/people';
		END IF;
	END LOOP;
	RETURN NEW;
END
$$;


ALTER FUNCTION "ConferenceManagement".conference_participants_update_trigger() OWNER TO salceson;

--
-- TOC entry 213 (class 1255 OID 30653)
-- Dependencies: 7 601
-- Name: conferencedays(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION conferencedays() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	conferencestartdate date;
	conferenceenddate date;
BEGIN
	EXECUTE 'SELECT "ConferenceStartDate"::date FROM "ConferenceManagement"."Conferences"
	WHERE "ConferenceID"=($1)' INTO conferencestartdate USING NEW."ConferenceID";
	EXECUTE 'SELECT "ConferenceEndDate"::date FROM "ConferenceManagement"."Conferences"
	WHERE "ConferenceID"=($1)' INTO conferenceenddate USING NEW."ConferenceID";
	IF NEW."ConferenceDayDate"<conferencestartdate OR NEW."ConferenceDayDate">conferenceenddate THEN
		RAISE EXCEPTION 'Conference day must be between start and end date of conference';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".conferencedays() OWNER TO salceson;

--
-- TOC entry 212 (class 1255 OID 30665)
-- Dependencies: 7 601
-- Name: discountstrigger(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION discountstrigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	conferencestartdate date;
BEGIN
	EXECUTE 'SELECT "ConferenceStartDate"::date FROM "ConferenceManagement"."Conferences"
	WHERE "ConferenceID"=($1)' INTO conferencestartdate USING NEW."ConferenceID";
	IF NEW."UntilDate">conferencestartdate THEN
		RAISE EXCEPTION 'Payment discount until date is later than conference start date';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".discountstrigger() OWNER TO salceson;

--
-- TOC entry 206 (class 1255 OID 30575)
-- Dependencies: 601 7
-- Name: getclientpayments(integer); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION getclientpayments(clientidarg integer) RETURNS TABLE(paymentid integer, reservationid integer, clientname text, clientcompanyname character varying, discount double precision, paymentdateto timestamp with time zone, paymentpaiddate timestamp with time zone, paymentamount money, paymentpaidamount money, clientid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM "ConferenceManagement"."ClientPaymentsView" WHERE "ClientID"=clientidarg;
END
$$;


ALTER FUNCTION "ConferenceManagement".getclientpayments(clientidarg integer) OWNER TO salceson;

--
-- TOC entry 205 (class 1255 OID 30536)
-- Dependencies: 601 7
-- Name: getconferencedayparticipants(integer); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION getconferencedayparticipants(conferencedayarg integer) RETURNS TABLE(conferencedayid integer, participantid integer, participantfirstname character varying, participantlastname character varying, studentidcard character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM "ConferenceManagement"."ConferenceDaysParticipantsView" WHERE "ConferenceDayID"=conferencedayarg;
END
$$;


ALTER FUNCTION "ConferenceManagement".getconferencedayparticipants(conferencedayarg integer) OWNER TO salceson;

--
-- TOC entry 204 (class 1255 OID 30531)
-- Dependencies: 601 7
-- Name: getworkshopparticipants(integer); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION getworkshopparticipants(argworkshopid integer) RETURNS TABLE(workshopid integer, workshopname character varying, participantid integer, participantfirstname character varying, participantlastname character varying, studentidcard character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY SELECT * FROM "ConferenceManagement"."WorkshopParticipantsView" WHERE "WorkshopID"=argworkshopid;
END
$$;


ALTER FUNCTION "ConferenceManagement".getworkshopparticipants(argworkshopid integer) OWNER TO salceson;

--
-- TOC entry 207 (class 1255 OID 30550)
-- Dependencies: 601 7
-- Name: trigger_insert_days_reservations(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION trigger_insert_days_reservations() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	drrecord RECORD;
	sumofpeople integer;
	maxsumofpeople integer;
BEGIN
	sumofpeople := 0;
	FOR drrecord IN
		SELECT "NumOfPeople" AS numofrecordpeople, "NumOfStudents" AS numofstudents
		FROM "ConferenceManagement"."ClientConferenceDaysReservations"
		WHERE "ReservationID" != NEW."ReservationID" AND "ConferenceDayID"=NEW."ConferenceDayID"
	LOOP
		sumofpeople := sumofpeople + drrecord.numofrecordpeople + drrecord.numofstudents;
	END LOOP;
	sumofpeople := sumofpeople + NEW."NumOfStudents" + NEW."NumOfPeople";
	EXECUTE 'SELECT "ConferenceDayCapacity" FROM "ConferenceManagement"."ConferenceDays" 
	WHERE "ConferenceDayID"=($1)' INTO maxsumofpeople USING NEW."ConferenceDayID";
	IF sumofpeople > maxsumofpeople THEN
		RAISE EXCEPTION 'Too much people';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".trigger_insert_days_reservations() OWNER TO salceson;

--
-- TOC entry 209 (class 1255 OID 30556)
-- Dependencies: 7 601
-- Name: trigger_insert_workshops_participants(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION trigger_insert_workshops_participants() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	maxsumofpeople integer;
	maxsumofstudents integer;
	sumofpeople integer;
	sumofstudents integer;
	isstudent boolean;
	checkfordayparticipation boolean;
	startdate timestamp with time zone;
	enddate timestamp with time zone;
BEGIN
	EXECUTE 'SELECT "NumOfPeople" FROM "ConferenceManagement"."ConferenceWorkshopsReservations" 
	WHERE "WorkshopID"=($1) AND "ReservationID"=($2) AND "ConferenceDayID"=($3)'
	INTO maxsumofpeople USING NEW."WorkshopID", NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT "NumOfStudents" FROM "ConferenceManagement"."ConferenceWorkshopsReservations" 
	WHERE "WorkshopID"=($1) AND "ReservationID"=($2) AND "ConferenceDayID"=($3)'
	INTO maxsumofstudents USING NEW."WorkshopID", NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT COUNT(*) FROM "ConferenceManagement"."WorkshopParticipants" wp
	JOIN "ConferenceManagement"."ConferenceParticipants" p ON
	wp."ParticipantID"=p."ParticipantID"
	WHERE wp."WorkshopID"=($1) AND wp."ReservationID"=($2) AND wp."ConferenceDayID"=($3)
	AND p."StudentIDCard" IS NULL' INTO sumofpeople USING
	NEW."WorkshopID", NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT COUNT(*) FROM "ConferenceManagement"."WorkshopParticipants" wp
	JOIN "ConferenceManagement"."ConferenceParticipants" p ON
	wp."ParticipantID"=p."ParticipantID"
	WHERE wp."WorkshopID"=($1) AND wp."ReservationID"=($2) AND wp."ConferenceDayID"=($3)
	AND p."StudentIDCard" IS NOT NULL' INTO sumofstudents USING
	NEW."WorkshopID", NEW."ReservationID", NEW."ConferenceDayID";
	EXECUTE 'SELECT ("StudentIDCard" IS NOT NULL)
	FROM "ConferenceManagement"."ConferenceParticipants" WHERE "ParticipantID"=($1)'
	INTO isstudent USING NEW."ParticipantID";
	IF isstudent THEN sumofstudents := sumofstudents + 1;
	ELSE sumofpeople := sumofpeople + 1;
	END IF;
	IF sumofpeople > maxsumofpeople OR sumofstudents > maxsumofstudents THEN
		RAISE EXCEPTION 'Too much people/students';
	END IF;
	EXECUTE 'SELECT (COUNT(*) = 1) FROM "ConferenceManagement"."ConferenceDaysParticipants" WHERE
	"ReservationID"=($1) AND "ConferenceDayID"=($2) AND "ParticipantID"=($3)' INTO
	checkfordayparticipation USING NEW."ReservationID", NEW."ConferenceDayID", NEW."ParticipantID";
	IF NOT checkfordayparticipation THEN
		RAISE EXCEPTION 'No participation in conference day';
	END IF;
	EXECUTE 'SELECT "WorkshopStartDate" FROM "ConferenceManagement"."ConferenceWorkshops" WHERE "WorkshopID" = ($1)'
	INTO startdate USING NEW."WorkshopID";
	EXECUTE 'SELECT "WorkshopEndDate" FROM "ConferenceManagement"."ConferenceWorkshops" WHERE "WorkshopID" = ($1)'
	INTO enddate USING NEW."WorkshopID";
	IF EXISTS (SELECT * FROM "ConferenceManagement"."ConferenceWorkshops" w JOIN "ConferenceManagement"."WorkshopParticipants" p
	ON p."WorkshopID"=w."WorkshopID" WHERE (w."WorkshopStartDate">=startdate AND w."WorkshopStartDate"<enddate)
	OR (w."WorkshopEndDate">startdate AND w."WorkshopEndDate"<=enddate) AND p."ParticipantID"=NEW."ParticipantID")
	THEN
		RAISE EXCEPTION 'Overlap with other workshop';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".trigger_insert_workshops_participants() OWNER TO salceson;

--
-- TOC entry 214 (class 1255 OID 30552)
-- Dependencies: 601 7
-- Name: trigger_insert_workshops_reservations(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION trigger_insert_workshops_reservations() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	drrecord RECORD;
	sumofpeople integer;
	maxsumofpeople integer;
BEGIN
	sumofpeople := 0;
	FOR drrecord IN
		SELECT "NumOfPeople" AS numofrecordpeople, "NumOfStudents" AS numofstudents
		FROM "ConferenceManagement"."WorkshopsDaysReservations"
		WHERE "ReservationID" != NEW."ReservationID" AND "WorkshopID"=NEW."WorkshopID"
	LOOP
		sumofpeople := sumofpeople + drrecord.numofrecordpeople + drrecord.numofstudents;
	END LOOP;
	sumofpeople := sumofpeople + NEW."NumOfStudents" + NEW."NumOfPeople";
	EXECUTE 'SELECT "WorkshopCapacity" FROM "ConferenceManagement"."ConferenceWorkshops" 
	WHERE "WorkshopID"=($1)' INTO maxsumofpeople USING NEW."WorkshopID";
	IF sumofpeople > maxsumofpeople THEN
		RAISE EXCEPTION 'Too much people';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".trigger_insert_workshops_reservations() OWNER TO salceson;

--
-- TOC entry 210 (class 1255 OID 30675)
-- Dependencies: 601 7
-- Name: workshopstrigger(); Type: FUNCTION; Schema: ConferenceManagement; Owner: salceson
--

CREATE FUNCTION workshopstrigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	conferencedaydate date;
	conferenceid integer;
BEGIN
	EXECUTE 'SELECT "ConferenceDayDate"::date FROM "ConferenceManagement"."ConferencesDays"
	WHERE "ConferenceID"=($1)' INTO conferencedaydate USING NEW."ConferenceDayID";
	EXECUTE 'SELECT "ConferenceID"::date FROM "ConferenceManagement"."ConferenceDays"
	WHERE "ConferenceDayID"=($1)' INTO conferenceid USING NEW."ConferenceDayID";
	IF NEW."WorkshopStartDate"::date != conferencedaydate OR "WorkshopEndDate"::date != conferencedaydate THEN
		RAISE EXCEPTION 'Workshop is not set to correct date.';
	END IF;
	IF NEW."ConferenceID" != conferenceid THEN
		RAISE EXCEPTION 'Workshop ConferenceID and ConferenceDay ConferenceID mismatch';
	END IF;
	RETURN NEW;
END
$_$;


ALTER FUNCTION "ConferenceManagement".workshopstrigger() OWNER TO salceson;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 21895)
-- Dependencies: 1923 7
-- Name: ConferenceWorkshops; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferenceWorkshops" (
    "WorkshopID" integer NOT NULL,
    "ConferenceID" integer NOT NULL,
    "ConferenceDayID" integer NOT NULL,
    "WorkshopStartDate" timestamp with time zone NOT NULL,
    "WorkshopEndDate" timestamp with time zone NOT NULL,
    "WorkshopCapacity" integer NOT NULL,
    "WorkshopConductor" character varying(100),
    "WorkshopName" character varying(255) NOT NULL,
    "WorkshopFee" money,
    "WorkshopStudentDiscount" double precision,
    CONSTRAINT "WorkshopsCK" CHECK (((((("WorkshopStartDate" > '2010-01-01 00:00:00+01'::timestamp with time zone) AND ("WorkshopEndDate" > "WorkshopStartDate")) AND ("WorkshopCapacity" > 0)) AND ("WorkshopFee" >= 'zł0,00'::money)) AND (("WorkshopStudentDiscount" >= (0)::double precision) AND ("WorkshopStudentDiscount" <= (1)::double precision))))
);


ALTER TABLE "ConferenceManagement"."ConferenceWorkshops" OWNER TO salceson;

--
-- TOC entry 181 (class 1259 OID 21905)
-- Dependencies: 7
-- Name: WorkshopParticipants; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "WorkshopParticipants" (
    "ParticipantID" integer NOT NULL,
    "ReservationID" integer NOT NULL,
    "WorkshopID" integer NOT NULL,
    "ConferenceDayID" integer NOT NULL
);


ALTER TABLE "ConferenceManagement"."WorkshopParticipants" OWNER TO salceson;

--
-- TOC entry 191 (class 1259 OID 30729)
-- Dependencies: 2131 7
-- Name: AttendanceAtWorkshopsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "AttendanceAtWorkshopsView" AS
    SELECT z."WorkshopName", avg(z."CurrentWorkshopAttendance") AS "Attendance" FROM (SELECT w."WorkshopName", ((count(p."ParticipantID"))::double precision / (w."WorkshopCapacity")::double precision) AS "CurrentWorkshopAttendance", w."WorkshopID" FROM ("ConferenceWorkshops" w JOIN "WorkshopParticipants" p ON ((w."WorkshopID" = p."WorkshopID"))) GROUP BY w."WorkshopName", w."WorkshopCapacity", w."WorkshopID" ORDER BY w."WorkshopName", ((count(p."ParticipantID"))::double precision / (w."WorkshopCapacity")::double precision) DESC) z GROUP BY z."WorkshopName" ORDER BY z."WorkshopName";


ALTER TABLE "ConferenceManagement"."AttendanceAtWorkshopsView" OWNER TO salceson;

--
-- TOC entry 162 (class 1259 OID 21848)
-- Dependencies: 1906 1907 7
-- Name: ClientConferenceDaysReservations; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ClientConferenceDaysReservations" (
    "ReservationID" integer NOT NULL,
    "ConferenceDayID" integer NOT NULL,
    "NumOfPeople" integer NOT NULL,
    "NumOfStudents" integer DEFAULT 0 NOT NULL,
    CONSTRAINT "ConferenceDaysReservationCK" CHECK (((("NumOfPeople" >= 0) AND ("NumOfStudents" >= 0)) AND (("NumOfPeople" + "NumOfStudents") > 0)))
);


ALTER TABLE "ConferenceManagement"."ClientConferenceDaysReservations" OWNER TO salceson;

--
-- TOC entry 163 (class 1259 OID 21852)
-- Dependencies: 1909 7
-- Name: ClientConferenceReservations; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ClientConferenceReservations" (
    "ReservationID" integer NOT NULL,
    "ClientID" integer NOT NULL,
    "ConferenceID" integer NOT NULL,
    "ReservationDate" timestamp with time zone NOT NULL,
    CONSTRAINT "ConferenceReservationsCK" CHECK (("ReservationDate" >= '2010-01-01 00:00:00+01'::timestamp with time zone))
);


ALTER TABLE "ConferenceManagement"."ClientConferenceReservations" OWNER TO salceson;

--
-- TOC entry 164 (class 1259 OID 21855)
-- Dependencies: 163 7
-- Name: ClientConferenceReservations_ReservationID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ClientConferenceReservations_ReservationID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ClientConferenceReservations_ReservationID_seq" OWNER TO salceson;

--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 164
-- Name: ClientConferenceReservations_ReservationID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ClientConferenceReservations_ReservationID_seq" OWNED BY "ClientConferenceReservations"."ReservationID";


--
-- TOC entry 165 (class 1259 OID 21857)
-- Dependencies: 1911 7
-- Name: ClientPayments; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ClientPayments" (
    "PaymentID" integer NOT NULL,
    "ReservationID" integer NOT NULL,
    "ClientID" integer NOT NULL,
    "DiscountID" integer,
    "PaymentDateTo" timestamp with time zone NOT NULL,
    "PaymentPaidDate" timestamp with time zone,
    "PaymentAmount" money NOT NULL,
    "PaymentPaidAmount" money,
    CONSTRAINT "PaymentsCheck" CHECK ((((("PaymentAmount" >= 'zł0,00'::money) AND ("PaymentPaidAmount" >= 'zł0,00'::money)) AND ("PaymentDateTo" >= '2010-01-01 00:00:00+01'::timestamp with time zone)) AND ("PaymentPaidDate" >= '2010-01-01 00:00:00+01'::timestamp with time zone)))
);


ALTER TABLE "ConferenceManagement"."ClientPayments" OWNER TO salceson;

--
-- TOC entry 167 (class 1259 OID 21862)
-- Dependencies: 1913 1914 7
-- Name: Clients; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "Clients" (
    "ClientID" integer NOT NULL,
    "ClientType" character(1) NOT NULL,
    "ClientFirstName" character varying(100) NOT NULL,
    "ClientLastName" character varying(100) NOT NULL,
    "ClientCompanyName" character varying(200),
    "ClientNIP" character varying(10),
    "ClientREGON" character varying(14),
    "ClientAddress" character varying(255) NOT NULL,
    "ClientCity" character varying(100) NOT NULL,
    "ClientCountry" character varying(100) NOT NULL,
    "ClientRegistrationDate" timestamp with time zone NOT NULL,
    "ClientPhone" character varying(40) NOT NULL,
    "ClientUsername" character varying(40) NOT NULL,
    "ClientPassword" character varying(128) NOT NULL,
    CONSTRAINT "ClientsCK" CHECK ((((char_length(("ClientNIP")::text) = 10) AND (char_length(("ClientREGON")::text) = ANY (ARRAY[9, 14]))) AND ("ClientRegistrationDate" > '2010-01-01 00:00:00+01'::timestamp with time zone))),
    CONSTRAINT "ClientsCheckCK" CHECK (("ClientType" = ANY (ARRAY['c'::bpchar, 'p'::bpchar])))
);


ALTER TABLE "ConferenceManagement"."Clients" OWNER TO salceson;

--
-- TOC entry 175 (class 1259 OID 21890)
-- Dependencies: 1921 7
-- Name: ConferencePaymentsDiscounts; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferencePaymentsDiscounts" (
    "DiscountID" integer NOT NULL,
    "ConferenceID" integer NOT NULL,
    "UntilDate" timestamp with time zone NOT NULL,
    "Discount" double precision NOT NULL,
    CONSTRAINT "DiscountsCK" CHECK ((("UntilDate" > '2010-01-01 00:00:00+01'::timestamp with time zone) AND (("Discount" >= (0)::double precision) AND ("Discount" <= (1)::double precision))))
);


ALTER TABLE "ConferenceManagement"."ConferencePaymentsDiscounts" OWNER TO salceson;

--
-- TOC entry 185 (class 1259 OID 30566)
-- Dependencies: 2125 7
-- Name: ClientPaymentsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "ClientPaymentsView" AS
    SELECT p."PaymentID", p."ReservationID", (((c."ClientLastName")::text || ' '::text) || (c."ClientFirstName")::text) AS clientname, c."ClientCompanyName", d."Discount", p."PaymentDateTo", p."PaymentPaidDate", p."PaymentAmount", p."PaymentPaidAmount", c."ClientID" FROM (("ClientPayments" p JOIN "ConferencePaymentsDiscounts" d ON ((p."DiscountID" = d."DiscountID"))) JOIN "Clients" c ON ((c."ClientID" = p."ClientID"))) ORDER BY (((c."ClientLastName")::text || ' '::text) || (c."ClientFirstName")::text), c."ClientCompanyName";


ALTER TABLE "ConferenceManagement"."ClientPaymentsView" OWNER TO salceson;

--
-- TOC entry 166 (class 1259 OID 21860)
-- Dependencies: 165 7
-- Name: ClientPayments_PaymentID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ClientPayments_PaymentID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ClientPayments_PaymentID_seq" OWNER TO salceson;

--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 166
-- Name: ClientPayments_PaymentID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ClientPayments_PaymentID_seq" OWNED BY "ClientPayments"."PaymentID";


--
-- TOC entry 168 (class 1259 OID 21869)
-- Dependencies: 7 167
-- Name: Clients_ClientID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "Clients_ClientID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."Clients_ClientID_seq" OWNER TO salceson;

--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 168
-- Name: Clients_ClientID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "Clients_ClientID_seq" OWNED BY "Clients"."ClientID";


--
-- TOC entry 169 (class 1259 OID 21871)
-- Dependencies: 1916 7
-- Name: Conference; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "Conference" (
    "ConferenceID" integer NOT NULL,
    "ConferenceName" character varying(100) NOT NULL,
    "ConferenceStartDate" date NOT NULL,
    "ConferenceEndDate" date NOT NULL,
    "ConferenceDescription" text,
    CONSTRAINT "ConferenceCK" CHECK ((("ConferenceEndDate" >= "ConferenceStartDate") AND ("ConferenceStartDate" >= '2010-01-01'::date)))
);


ALTER TABLE "ConferenceManagement"."Conference" OWNER TO salceson;

--
-- TOC entry 170 (class 1259 OID 21877)
-- Dependencies: 1918 7
-- Name: ConferenceDays; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferenceDays" (
    "ConferenceDayID" integer NOT NULL,
    "ConferenceID" integer NOT NULL,
    "ConferenceDay" integer NOT NULL,
    "ConferenceDayDate" date NOT NULL,
    "ConferenceDayFee" money NOT NULL,
    "ConferenceDayStudentDiscount" double precision NOT NULL,
    "ConferenceDayCapacity" integer,
    CONSTRAINT "ConferenceDaysCK" CHECK (((((("ConferenceDay" >= 0) AND ("ConferenceDayDate" >= '2010-01-01'::date)) AND ("ConferenceDayCapacity" > 0)) AND (("ConferenceDayStudentDiscount" >= (0)::double precision) AND ("ConferenceDayStudentDiscount" <= (1)::double precision))) AND ("ConferenceDayFee" >= 'zł0,00'::money)))
);


ALTER TABLE "ConferenceManagement"."ConferenceDays" OWNER TO salceson;

--
-- TOC entry 171 (class 1259 OID 21880)
-- Dependencies: 7
-- Name: ConferenceDaysParticipants; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferenceDaysParticipants" (
    "ParticipantID" integer NOT NULL,
    "ConferenceDayID" integer NOT NULL,
    "ReservationID" integer NOT NULL
);


ALTER TABLE "ConferenceManagement"."ConferenceDaysParticipants" OWNER TO salceson;

--
-- TOC entry 173 (class 1259 OID 21885)
-- Dependencies: 7
-- Name: ConferenceParticipants; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferenceParticipants" (
    "ParticipantID" integer NOT NULL,
    "ClientID" integer NOT NULL,
    "ParticipantFirstName" character varying(50) NOT NULL,
    "ParticipantLastName" character varying(60) NOT NULL,
    "StudentIDCard" character varying(30),
    "ReservationID" integer NOT NULL
);


ALTER TABLE "ConferenceManagement"."ConferenceParticipants" OWNER TO salceson;

--
-- TOC entry 183 (class 1259 OID 30532)
-- Dependencies: 2123 7
-- Name: ConferenceDaysParticipantsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "ConferenceDaysParticipantsView" AS
    SELECT dp."ConferenceDayID", p."ParticipantID", p."ParticipantFirstName", p."ParticipantLastName", p."StudentIDCard", c."ClientCompanyName" FROM (("ConferenceDaysParticipants" dp JOIN "ConferenceParticipants" p ON ((p."ParticipantID" = dp."ParticipantID"))) JOIN "Clients" c ON ((c."ClientID" = p."ClientID")));


ALTER TABLE "ConferenceManagement"."ConferenceDaysParticipantsView" OWNER TO salceson;

--
-- TOC entry 172 (class 1259 OID 21883)
-- Dependencies: 170 7
-- Name: ConferenceDays_ConferenceDayID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ConferenceDays_ConferenceDayID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ConferenceDays_ConferenceDayID_seq" OWNER TO salceson;

--
-- TOC entry 2173 (class 0 OID 0)
-- Dependencies: 172
-- Name: ConferenceDays_ConferenceDayID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ConferenceDays_ConferenceDayID_seq" OWNED BY "ConferenceDays"."ConferenceDayID";


--
-- TOC entry 174 (class 1259 OID 21888)
-- Dependencies: 7 173
-- Name: ConferenceParticipants_ParticipantID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ConferenceParticipants_ParticipantID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ConferenceParticipants_ParticipantID_seq" OWNER TO salceson;

--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 174
-- Name: ConferenceParticipants_ParticipantID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ConferenceParticipants_ParticipantID_seq" OWNED BY "ConferenceParticipants"."ParticipantID";


--
-- TOC entry 176 (class 1259 OID 21893)
-- Dependencies: 7 175
-- Name: ConferencePaymentsDiscounts_DiscountID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ConferencePaymentsDiscounts_DiscountID_seq" OWNER TO salceson;

--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 176
-- Name: ConferencePaymentsDiscounts_DiscountID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" OWNED BY "ConferencePaymentsDiscounts"."DiscountID";


--
-- TOC entry 179 (class 1259 OID 21900)
-- Dependencies: 1924 7
-- Name: ConferenceWorkshopsReservations; Type: TABLE; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE TABLE "ConferenceWorkshopsReservations" (
    "WorkshopID" integer NOT NULL,
    "ConferenceDayID" integer NOT NULL,
    "ReservationID" integer NOT NULL,
    "NumOfPeople" integer NOT NULL,
    "NumOfStudents" integer NOT NULL,
    CONSTRAINT "WorkshopReservationsCK" CHECK (((("NumOfPeople" >= 0) AND ("NumOfStudents" >= 0)) AND (("NumOfPeople" + "NumOfStudents") > 0)))
);


ALTER TABLE "ConferenceManagement"."ConferenceWorkshopsReservations" OWNER TO salceson;

--
-- TOC entry 178 (class 1259 OID 21898)
-- Dependencies: 177 7
-- Name: ConferenceWorkshops_WorkshopID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "ConferenceWorkshops_WorkshopID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."ConferenceWorkshops_WorkshopID_seq" OWNER TO salceson;

--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 178
-- Name: ConferenceWorkshops_WorkshopID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "ConferenceWorkshops_WorkshopID_seq" OWNED BY "ConferenceWorkshops"."WorkshopID";


--
-- TOC entry 180 (class 1259 OID 21903)
-- Dependencies: 169 7
-- Name: Conference_ConferenceID_seq; Type: SEQUENCE; Schema: ConferenceManagement; Owner: salceson
--

CREATE SEQUENCE "Conference_ConferenceID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ConferenceManagement"."Conference_ConferenceID_seq" OWNER TO salceson;

--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 180
-- Name: Conference_ConferenceID_seq; Type: SEQUENCE OWNED BY; Schema: ConferenceManagement; Owner: salceson
--

ALTER SEQUENCE "Conference_ConferenceID_seq" OWNED BY "Conference"."ConferenceID";


--
-- TOC entry 189 (class 1259 OID 30708)
-- Dependencies: 2129 7
-- Name: MonthlyEearningReportView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "MonthlyEearningReportView" AS
    SELECT (date_part('year'::text, p."PaymentPaidDate"))::integer AS year, (date_part('month'::text, p."PaymentPaidDate"))::integer AS month, sum(p."PaymentPaidAmount") AS "Total earnings" FROM "ClientPayments" p GROUP BY (date_part('year'::text, p."PaymentPaidDate"))::integer, (date_part('month'::text, p."PaymentPaidDate"))::integer ORDER BY (date_part('year'::text, p."PaymentPaidDate"))::integer DESC, (date_part('month'::text, p."PaymentPaidDate"))::integer DESC;


ALTER TABLE "ConferenceManagement"."MonthlyEearningReportView" OWNER TO salceson;

--
-- TOC entry 187 (class 1259 OID 30696)
-- Dependencies: 2127 7
-- Name: MostEarningConferencesView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "MostEarningConferencesView" AS
    SELECT c."ConferenceName", sum(p."PaymentAmount") AS "Total earnings" FROM (("ClientConferenceReservations" r JOIN "Conference" c ON ((c."ConferenceID" = r."ConferenceID"))) JOIN "ClientPayments" p ON ((r."ReservationID" = p."ReservationID"))) GROUP BY c."ConferenceName" ORDER BY sum(p."PaymentAmount") DESC, c."ConferenceName" LIMIT 50;


ALTER TABLE "ConferenceManagement"."MostEarningConferencesView" OWNER TO salceson;

--
-- TOC entry 188 (class 1259 OID 30701)
-- Dependencies: 2128 7
-- Name: MostEarningWorkshopsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "MostEarningWorkshopsView" AS
    SELECT w."WorkshopName", sum(((((COALESCE(d."Discount", (1)::double precision) * ((r."NumOfPeople")::double precision + (COALESCE(w."WorkshopStudentDiscount", (1)::double precision) * (r."NumOfStudents")::double precision))) * w."WorkshopFee"))::numeric)::money) AS "Total earnings" FROM ((("ConferenceWorkshopsReservations" r JOIN "ConferenceWorkshops" w ON ((w."WorkshopID" = r."WorkshopID"))) JOIN "ClientPayments" p ON ((r."ReservationID" = p."ReservationID"))) JOIN "ConferencePaymentsDiscounts" d ON ((d."DiscountID" = p."DiscountID"))) GROUP BY w."WorkshopName" ORDER BY sum(((((COALESCE(d."Discount", (1)::double precision) * ((r."NumOfPeople")::double precision + (COALESCE(w."WorkshopStudentDiscount", (1)::double precision) * (r."NumOfStudents")::double precision))) * w."WorkshopFee"))::numeric)::money) DESC, w."WorkshopName" LIMIT 50;


ALTER TABLE "ConferenceManagement"."MostEarningWorkshopsView" OWNER TO salceson;

--
-- TOC entry 184 (class 1259 OID 30543)
-- Dependencies: 2124 7
-- Name: MostOftenCustomersView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "MostOftenCustomersView" AS
    SELECT c."ClientID", c."ClientFirstName", c."ClientLastName", c."ClientCompanyName", count(r."ReservationID") AS "ReservationsCount" FROM ("ClientConferenceReservations" r JOIN "Clients" c ON ((r."ClientID" = c."ClientID"))) GROUP BY c."ClientID", c."ClientFirstName", c."ClientLastName", c."ClientCompanyName" ORDER BY count(r."ReservationID") DESC, c."ClientLastName", c."ClientFirstName" LIMIT 50;


ALTER TABLE "ConferenceManagement"."MostOftenCustomersView" OWNER TO salceson;

--
-- TOC entry 186 (class 1259 OID 30692)
-- Dependencies: 2126 7
-- Name: MostPopularWorkshopsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "MostPopularWorkshopsView" AS
    SELECT w."WorkshopName", sum((r."NumOfPeople" + r."NumOfStudents")) AS "Number of participants" FROM ("ConferenceWorkshopsReservations" r JOIN "ConferenceWorkshops" w ON ((w."WorkshopID" = r."WorkshopID"))) GROUP BY w."WorkshopName" ORDER BY sum((r."NumOfPeople" + r."NumOfStudents")) DESC, w."WorkshopName" LIMIT 50;


ALTER TABLE "ConferenceManagement"."MostPopularWorkshopsView" OWNER TO salceson;

--
-- TOC entry 190 (class 1259 OID 30712)
-- Dependencies: 2130 7
-- Name: PaymentsNotPaidOnTimeView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "PaymentsNotPaidOnTimeView" AS
    SELECT "ClientPayments"."PaymentID", "ClientPayments"."ReservationID", "ClientPayments"."ClientID", "ClientPayments"."DiscountID", "ClientPayments"."PaymentDateTo", "ClientPayments"."PaymentPaidDate", "ClientPayments"."PaymentAmount", "ClientPayments"."PaymentPaidAmount" FROM "ClientPayments" WHERE ((COALESCE("ClientPayments"."PaymentPaidAmount", (0)::money) < "ClientPayments"."PaymentAmount") AND ("ClientPayments"."PaymentDateTo" < now()));


ALTER TABLE "ConferenceManagement"."PaymentsNotPaidOnTimeView" OWNER TO salceson;

--
-- TOC entry 182 (class 1259 OID 30522)
-- Dependencies: 2122 7
-- Name: WorkshopParticipantsView; Type: VIEW; Schema: ConferenceManagement; Owner: salceson
--

CREATE VIEW "WorkshopParticipantsView" AS
    SELECT wp."WorkshopID", w."WorkshopName", p."ParticipantID", p."ParticipantFirstName", p."ParticipantLastName", p."StudentIDCard", c."ClientCompanyName" FROM ((("WorkshopParticipants" wp JOIN "ConferenceWorkshops" w ON ((w."WorkshopID" = wp."WorkshopID"))) JOIN "ConferenceParticipants" p ON ((wp."ParticipantID" = p."ParticipantID"))) JOIN "Clients" c ON ((c."ClientID" = p."ClientID")));


ALTER TABLE "ConferenceManagement"."WorkshopParticipantsView" OWNER TO salceson;

--
-- TOC entry 1908 (class 2604 OID 21908)
-- Dependencies: 164 163
-- Name: ReservationID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientConferenceReservations" ALTER COLUMN "ReservationID" SET DEFAULT nextval('"ClientConferenceReservations_ReservationID_seq"'::regclass);


--
-- TOC entry 1910 (class 2604 OID 21909)
-- Dependencies: 166 165
-- Name: PaymentID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientPayments" ALTER COLUMN "PaymentID" SET DEFAULT nextval('"ClientPayments_PaymentID_seq"'::regclass);


--
-- TOC entry 1912 (class 2604 OID 21910)
-- Dependencies: 168 167
-- Name: ClientID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "Clients" ALTER COLUMN "ClientID" SET DEFAULT nextval('"Clients_ClientID_seq"'::regclass);


--
-- TOC entry 1915 (class 2604 OID 21911)
-- Dependencies: 180 169
-- Name: ConferenceID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "Conference" ALTER COLUMN "ConferenceID" SET DEFAULT nextval('"Conference_ConferenceID_seq"'::regclass);


--
-- TOC entry 1917 (class 2604 OID 21912)
-- Dependencies: 172 170
-- Name: ConferenceDayID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceDays" ALTER COLUMN "ConferenceDayID" SET DEFAULT nextval('"ConferenceDays_ConferenceDayID_seq"'::regclass);


--
-- TOC entry 1919 (class 2604 OID 21913)
-- Dependencies: 174 173
-- Name: ParticipantID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceParticipants" ALTER COLUMN "ParticipantID" SET DEFAULT nextval('"ConferenceParticipants_ParticipantID_seq"'::regclass);


--
-- TOC entry 1920 (class 2604 OID 21914)
-- Dependencies: 176 175
-- Name: DiscountID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferencePaymentsDiscounts" ALTER COLUMN "DiscountID" SET DEFAULT nextval('"ConferencePaymentsDiscounts_DiscountID_seq"'::regclass);


--
-- TOC entry 1922 (class 2604 OID 21915)
-- Dependencies: 178 177
-- Name: WorkshopID; Type: DEFAULT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshops" ALTER COLUMN "WorkshopID" SET DEFAULT nextval('"ConferenceWorkshops_WorkshopID_seq"'::regclass);


--
-- TOC entry 1939 (class 2606 OID 21917)
-- Dependencies: 167 167 2133
-- Name: ClientPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "Clients"
    ADD CONSTRAINT "ClientPK" PRIMARY KEY ("ClientID");


--
-- TOC entry 1949 (class 2606 OID 21919)
-- Dependencies: 167 167 2133
-- Name: ClientsUnique; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "Clients"
    ADD CONSTRAINT "ClientsUnique" UNIQUE ("ClientUsername");


--
-- TOC entry 1961 (class 2606 OID 21921)
-- Dependencies: 170 170 2133
-- Name: ConferenceDaysPKConstraint; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferenceDays"
    ADD CONSTRAINT "ConferenceDaysPKConstraint" PRIMARY KEY ("ConferenceDayID");


--
-- TOC entry 1963 (class 2606 OID 21923)
-- Dependencies: 171 171 171 171 2133
-- Name: ConferenceDaysParticipantsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferenceDaysParticipants"
    ADD CONSTRAINT "ConferenceDaysParticipantsPK" PRIMARY KEY ("ParticipantID", "ConferenceDayID", "ReservationID");


--
-- TOC entry 1926 (class 2606 OID 21925)
-- Dependencies: 162 162 162 2133
-- Name: ConferenceDaysReservationPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ClientConferenceDaysReservations"
    ADD CONSTRAINT "ConferenceDaysReservationPK" PRIMARY KEY ("ConferenceDayID", "ReservationID");


--
-- TOC entry 1955 (class 2606 OID 21927)
-- Dependencies: 169 169 2133
-- Name: ConferencePKConstraint; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "Conference"
    ADD CONSTRAINT "ConferencePKConstraint" PRIMARY KEY ("ConferenceID");


--
-- TOC entry 1969 (class 2606 OID 21929)
-- Dependencies: 173 173 2133
-- Name: ConferenceParticipantsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferenceParticipants"
    ADD CONSTRAINT "ConferenceParticipantsPK" PRIMARY KEY ("ParticipantID");


--
-- TOC entry 1974 (class 2606 OID 21931)
-- Dependencies: 175 175 2133
-- Name: DiscountsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferencePaymentsDiscounts"
    ADD CONSTRAINT "DiscountsPK" PRIMARY KEY ("DiscountID");


--
-- TOC entry 1934 (class 2606 OID 21933)
-- Dependencies: 165 165 2133
-- Name: PaymentPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ClientPayments"
    ADD CONSTRAINT "PaymentPK" PRIMARY KEY ("PaymentID");


--
-- TOC entry 1932 (class 2606 OID 21935)
-- Dependencies: 163 163 2133
-- Name: ReservationsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ClientConferenceReservations"
    ADD CONSTRAINT "ReservationsPK" PRIMARY KEY ("ReservationID");


--
-- TOC entry 1989 (class 2606 OID 21937)
-- Dependencies: 181 181 181 181 181 2133
-- Name: WorkshopParticipantsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "WorkshopParticipants"
    ADD CONSTRAINT "WorkshopParticipantsPK" PRIMARY KEY ("ParticipantID", "ReservationID", "WorkshopID", "ConferenceDayID");


--
-- TOC entry 1987 (class 2606 OID 21939)
-- Dependencies: 179 179 179 179 2133
-- Name: WorkshopReservationsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferenceWorkshopsReservations"
    ADD CONSTRAINT "WorkshopReservationsPK" PRIMARY KEY ("WorkshopID", "ConferenceDayID", "ReservationID");


--
-- TOC entry 1982 (class 2606 OID 21941)
-- Dependencies: 177 177 2133
-- Name: WorkshopsPK; Type: CONSTRAINT; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

ALTER TABLE ONLY "ConferenceWorkshops"
    ADD CONSTRAINT "WorkshopsPK" PRIMARY KEY ("WorkshopID");


--
-- TOC entry 1940 (class 1259 OID 30632)
-- Dependencies: 167 2133
-- Name: ClientsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex1" ON "Clients" USING btree ("ClientID");


--
-- TOC entry 1941 (class 1259 OID 30635)
-- Dependencies: 167 2133
-- Name: ClientsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex2" ON "Clients" USING btree ("ClientType");


--
-- TOC entry 1942 (class 1259 OID 30634)
-- Dependencies: 167 2133
-- Name: ClientsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex3" ON "Clients" USING btree ("ClientUsername");


--
-- TOC entry 1943 (class 1259 OID 30636)
-- Dependencies: 167 2133
-- Name: ClientsIndex4; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex4" ON "Clients" USING btree ("ClientFirstName");


--
-- TOC entry 1944 (class 1259 OID 30637)
-- Dependencies: 167 2133
-- Name: ClientsIndex5; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex5" ON "Clients" USING btree ("ClientLastName");


--
-- TOC entry 1945 (class 1259 OID 30638)
-- Dependencies: 167 2133
-- Name: ClientsIndex6; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex6" ON "Clients" USING btree ("ClientCompanyName");


--
-- TOC entry 1946 (class 1259 OID 30639)
-- Dependencies: 167 2133
-- Name: ClientsIndex7; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex7" ON "Clients" USING btree ("ClientNIP");


--
-- TOC entry 1947 (class 1259 OID 30640)
-- Dependencies: 167 2133
-- Name: ClientsIndex8; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ClientsIndex8" ON "Clients" USING btree ("ClientREGON");


--
-- TOC entry 1956 (class 1259 OID 30647)
-- Dependencies: 170 2133
-- Name: ConferenceDaysIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceDaysIndex1" ON "ConferenceDays" USING btree ("ConferenceDayID");


--
-- TOC entry 1957 (class 1259 OID 30648)
-- Dependencies: 170 2133
-- Name: ConferenceDaysIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceDaysIndex2" ON "ConferenceDays" USING btree ("ConferenceID");


--
-- TOC entry 1958 (class 1259 OID 30649)
-- Dependencies: 170 2133
-- Name: ConferenceDaysIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceDaysIndex3" ON "ConferenceDays" USING btree ("ConferenceDay");


--
-- TOC entry 1959 (class 1259 OID 30650)
-- Dependencies: 170 2133
-- Name: ConferenceDaysIndex4; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceDaysIndex4" ON "ConferenceDays" USING btree ("ConferenceDayDate");


--
-- TOC entry 1927 (class 1259 OID 30620)
-- Dependencies: 162 162 2133
-- Name: ConferenceDaysReservationsIndex; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceDaysReservationsIndex" ON "ClientConferenceDaysReservations" USING btree ("ReservationID", "ConferenceDayID");


--
-- TOC entry 1950 (class 1259 OID 30642)
-- Dependencies: 169 2133
-- Name: ConferenceIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceIndex1" ON "Conference" USING btree ("ConferenceID");


--
-- TOC entry 1951 (class 1259 OID 30643)
-- Dependencies: 169 2133
-- Name: ConferenceIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceIndex2" ON "Conference" USING btree ("ConferenceName");


--
-- TOC entry 1952 (class 1259 OID 30644)
-- Dependencies: 169 2133
-- Name: ConferenceIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceIndex3" ON "Conference" USING btree ("ConferenceStartDate");


--
-- TOC entry 1953 (class 1259 OID 30645)
-- Dependencies: 169 2133
-- Name: ConferenceIndex4; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceIndex4" ON "Conference" USING btree ("ConferenceEndDate");


--
-- TOC entry 1964 (class 1259 OID 30655)
-- Dependencies: 173 2133
-- Name: ConferenceParticipantsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceParticipantsIndex1" ON "ConferenceParticipants" USING btree ("ParticipantID");


--
-- TOC entry 1965 (class 1259 OID 30656)
-- Dependencies: 173 2133
-- Name: ConferenceParticipantsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceParticipantsIndex2" ON "ConferenceParticipants" USING btree ("ClientID");


--
-- TOC entry 1966 (class 1259 OID 30657)
-- Dependencies: 173 2133
-- Name: ConferenceParticipantsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceParticipantsIndex3" ON "ConferenceParticipants" USING btree ("ParticipantFirstName");


--
-- TOC entry 1967 (class 1259 OID 30658)
-- Dependencies: 173 2133
-- Name: ConferenceParticipantsIndex4; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceParticipantsIndex4" ON "ConferenceParticipants" USING btree ("ParticipantLastName");


--
-- TOC entry 1928 (class 1259 OID 30621)
-- Dependencies: 163 2133
-- Name: ConferenceReservationsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceReservationsIndex1" ON "ClientConferenceReservations" USING btree ("ReservationID");


--
-- TOC entry 1929 (class 1259 OID 30622)
-- Dependencies: 163 2133
-- Name: ConferenceReservationsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceReservationsIndex2" ON "ClientConferenceReservations" USING btree ("ClientID");


--
-- TOC entry 1930 (class 1259 OID 30623)
-- Dependencies: 163 2133
-- Name: ConferenceReservationsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "ConferenceReservationsIndex3" ON "ClientConferenceReservations" USING btree ("ConferenceID");


--
-- TOC entry 1970 (class 1259 OID 30660)
-- Dependencies: 175 2133
-- Name: DiscountsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "DiscountsIndex1" ON "ConferencePaymentsDiscounts" USING btree ("DiscountID");


--
-- TOC entry 1971 (class 1259 OID 30661)
-- Dependencies: 175 2133
-- Name: DiscountsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "DiscountsIndex2" ON "ConferencePaymentsDiscounts" USING btree ("ConferenceID");


--
-- TOC entry 1972 (class 1259 OID 30662)
-- Dependencies: 175 2133
-- Name: DiscountsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "DiscountsIndex3" ON "ConferencePaymentsDiscounts" USING btree ("UntilDate");


--
-- TOC entry 1935 (class 1259 OID 30627)
-- Dependencies: 165 2133
-- Name: PaymentsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "PaymentsIndex1" ON "ClientPayments" USING btree ("PaymentID");


--
-- TOC entry 1936 (class 1259 OID 30628)
-- Dependencies: 165 2133
-- Name: PaymentsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "PaymentsIndex2" ON "ClientPayments" USING btree ("ClientID");


--
-- TOC entry 1937 (class 1259 OID 30629)
-- Dependencies: 165 2133
-- Name: PaymentsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "PaymentsIndex3" ON "ClientPayments" USING btree ("PaymentDateTo");


--
-- TOC entry 1983 (class 1259 OID 30678)
-- Dependencies: 179 2133
-- Name: WorkshopReservationsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopReservationsIndex1" ON "ConferenceWorkshopsReservations" USING btree ("WorkshopID");


--
-- TOC entry 1984 (class 1259 OID 30679)
-- Dependencies: 179 2133
-- Name: WorkshopReservationsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopReservationsIndex2" ON "ConferenceWorkshopsReservations" USING btree ("ConferenceDayID");


--
-- TOC entry 1985 (class 1259 OID 30680)
-- Dependencies: 179 2133
-- Name: WorkshopReservationsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopReservationsIndex3" ON "ConferenceWorkshopsReservations" USING btree ("ReservationID");


--
-- TOC entry 1975 (class 1259 OID 30668)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex1; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex1" ON "ConferenceWorkshops" USING btree ("WorkshopID");


--
-- TOC entry 1976 (class 1259 OID 30669)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex2; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex2" ON "ConferenceWorkshops" USING btree ("ConferenceID");


--
-- TOC entry 1977 (class 1259 OID 30670)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex3; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex3" ON "ConferenceWorkshops" USING btree ("ConferenceDayID");


--
-- TOC entry 1978 (class 1259 OID 30671)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex4; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex4" ON "ConferenceWorkshops" USING btree ("WorkshopStartDate");


--
-- TOC entry 1979 (class 1259 OID 30672)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex5; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex5" ON "ConferenceWorkshops" USING btree ("WorkshopEndDate");


--
-- TOC entry 1980 (class 1259 OID 30673)
-- Dependencies: 177 2133
-- Name: WorkshopsIndex6; Type: INDEX; Schema: ConferenceManagement; Owner: salceson; Tablespace: 
--

CREATE INDEX "WorkshopsIndex6" ON "ConferenceWorkshops" USING btree ("WorkshopConductor");


--
-- TOC entry 2013 (class 2620 OID 30551)
-- Dependencies: 207 162 162 162 2133
-- Name: ConferenceDaysReservationsTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "ConferenceDaysReservationsTrigger" BEFORE INSERT OR UPDATE OF "NumOfPeople", "NumOfStudents" ON "ClientConferenceDaysReservations" FOR EACH ROW EXECUTE PROCEDURE trigger_insert_days_reservations();


--
-- TOC entry 2014 (class 2620 OID 30654)
-- Dependencies: 170 213 170 2133
-- Name: ConferenceDaysTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "ConferenceDaysTrigger" BEFORE INSERT OR UPDATE OF "ConferenceDayDate" ON "ConferenceDays" FOR EACH ROW EXECUTE PROCEDURE conferencedays();


--
-- TOC entry 2020 (class 2620 OID 30557)
-- Dependencies: 209 181 2133
-- Name: ConferenceWorkshopsParticipantsTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "ConferenceWorkshopsParticipantsTrigger" BEFORE INSERT ON "WorkshopParticipants" FOR EACH ROW EXECUTE PROCEDURE trigger_insert_workshops_participants();


--
-- TOC entry 2019 (class 2620 OID 30553)
-- Dependencies: 214 179 179 179 2133
-- Name: ConferenceWorkshopsReservationsTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "ConferenceWorkshopsReservationsTrigger" BEFORE INSERT OR UPDATE OF "NumOfPeople", "NumOfStudents" ON "ConferenceWorkshopsReservations" FOR EACH ROW EXECUTE PROCEDURE trigger_insert_workshops_reservations();


--
-- TOC entry 2017 (class 2620 OID 30666)
-- Dependencies: 212 175 175 2133
-- Name: DiscountsTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "DiscountsTrigger" BEFORE INSERT OR UPDATE OF "UntilDate" ON "ConferencePaymentsDiscounts" FOR EACH ROW EXECUTE PROCEDURE discountstrigger();


--
-- TOC entry 2018 (class 2620 OID 30676)
-- Dependencies: 177 210 177 177 177 2133
-- Name: WorkshopsTrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER "WorkshopsTrigger" BEFORE INSERT OR UPDATE OF "WorkshopStartDate", "WorkshopEndDate", "ConferenceID" ON "ConferenceWorkshops" FOR EACH ROW EXECUTE PROCEDURE workshopstrigger();


--
-- TOC entry 2015 (class 2620 OID 30560)
-- Dependencies: 171 211 2133
-- Name: conferencedaysparticipantstrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER conferencedaysparticipantstrigger BEFORE INSERT ON "ConferenceDaysParticipants" FOR EACH ROW EXECUTE PROCEDURE conference_days_participants_inserting_trigger();


--
-- TOC entry 2016 (class 2620 OID 30562)
-- Dependencies: 173 173 208 2133
-- Name: conferenceparticipantsupdatingtrigger; Type: TRIGGER; Schema: ConferenceManagement; Owner: salceson
--

CREATE TRIGGER conferenceparticipantsupdatingtrigger BEFORE UPDATE OF "StudentIDCard" ON "ConferenceParticipants" FOR EACH ROW EXECUTE PROCEDURE conference_participants_update_trigger();


--
-- TOC entry 1997 (class 2606 OID 21942)
-- Dependencies: 1954 170 169 2133
-- Name: ConferenceDaysConferenceIDFKConstraint; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceDays"
    ADD CONSTRAINT "ConferenceDaysConferenceIDFKConstraint" FOREIGN KEY ("ConferenceID") REFERENCES "Conference"("ConferenceID") ON DELETE CASCADE;


--
-- TOC entry 1998 (class 2606 OID 21947)
-- Dependencies: 173 171 1968 2133
-- Name: ConferenceDaysParticipantsFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceDaysParticipants"
    ADD CONSTRAINT "ConferenceDaysParticipantsFK1" FOREIGN KEY ("ParticipantID") REFERENCES "ConferenceParticipants"("ParticipantID") ON DELETE CASCADE;


--
-- TOC entry 1999 (class 2606 OID 21952)
-- Dependencies: 1960 170 171 2133
-- Name: ConferenceDaysParticipantsFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceDaysParticipants"
    ADD CONSTRAINT "ConferenceDaysParticipantsFK2" FOREIGN KEY ("ConferenceDayID") REFERENCES "ConferenceDays"("ConferenceDayID") ON DELETE CASCADE;


--
-- TOC entry 2000 (class 2606 OID 21957)
-- Dependencies: 1931 163 171 2133
-- Name: ConferenceDaysParticipantsFK3; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceDaysParticipants"
    ADD CONSTRAINT "ConferenceDaysParticipantsFK3" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID") ON DELETE CASCADE;


--
-- TOC entry 1990 (class 2606 OID 30482)
-- Dependencies: 163 162 1931 2133
-- Name: ConferenceDaysReservationFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientConferenceDaysReservations"
    ADD CONSTRAINT "ConferenceDaysReservationFK1" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID") ON DELETE CASCADE;


--
-- TOC entry 1991 (class 2606 OID 30487)
-- Dependencies: 170 1960 162 2133
-- Name: ConferenceDaysReservationFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientConferenceDaysReservations"
    ADD CONSTRAINT "ConferenceDaysReservationFK2" FOREIGN KEY ("ConferenceDayID") REFERENCES "ConferenceDays"("ConferenceDayID") ON DELETE CASCADE;


--
-- TOC entry 2001 (class 2606 OID 21972)
-- Dependencies: 167 1938 173 2133
-- Name: ConferenceParticipantsFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceParticipants"
    ADD CONSTRAINT "ConferenceParticipantsFK1" FOREIGN KEY ("ClientID") REFERENCES "Clients"("ClientID") ON DELETE CASCADE;


--
-- TOC entry 2002 (class 2606 OID 21977)
-- Dependencies: 163 1931 173 2133
-- Name: ConferenceParticipantsFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceParticipants"
    ADD CONSTRAINT "ConferenceParticipantsFK2" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID") ON DELETE CASCADE;


--
-- TOC entry 2006 (class 2606 OID 21982)
-- Dependencies: 179 163 1931 2133
-- Name: ConferenceWorkshopsReservationsFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshopsReservations"
    ADD CONSTRAINT "ConferenceWorkshopsReservationsFK1" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID");


--
-- TOC entry 2007 (class 2606 OID 21987)
-- Dependencies: 1960 179 170 2133
-- Name: ConferenceWorkshopsReservationsFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshopsReservations"
    ADD CONSTRAINT "ConferenceWorkshopsReservationsFK2" FOREIGN KEY ("ConferenceDayID") REFERENCES "ConferenceDays"("ConferenceDayID");


--
-- TOC entry 2008 (class 2606 OID 21992)
-- Dependencies: 1981 179 177 2133
-- Name: ConferenceWorkshopsReservationsFK3; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshopsReservations"
    ADD CONSTRAINT "ConferenceWorkshopsReservationsFK3" FOREIGN KEY ("WorkshopID") REFERENCES "ConferenceWorkshops"("WorkshopID");


--
-- TOC entry 2003 (class 2606 OID 30517)
-- Dependencies: 1954 175 169 2133
-- Name: DiscountsFK; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferencePaymentsDiscounts"
    ADD CONSTRAINT "DiscountsFK" FOREIGN KEY ("ConferenceID") REFERENCES "Conference"("ConferenceID") ON DELETE CASCADE;


--
-- TOC entry 1994 (class 2606 OID 30502)
-- Dependencies: 163 1931 165 2133
-- Name: PaymentFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientPayments"
    ADD CONSTRAINT "PaymentFK1" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID") ON DELETE CASCADE;


--
-- TOC entry 1995 (class 2606 OID 30507)
-- Dependencies: 167 1938 165 2133
-- Name: PaymentFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientPayments"
    ADD CONSTRAINT "PaymentFK2" FOREIGN KEY ("ClientID") REFERENCES "Clients"("ClientID") ON DELETE CASCADE;


--
-- TOC entry 1996 (class 2606 OID 30512)
-- Dependencies: 165 175 1973 2133
-- Name: PaymentFK3; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientPayments"
    ADD CONSTRAINT "PaymentFK3" FOREIGN KEY ("DiscountID") REFERENCES "ConferencePaymentsDiscounts"("DiscountID") ON DELETE CASCADE;


--
-- TOC entry 1992 (class 2606 OID 30492)
-- Dependencies: 1938 163 167 2133
-- Name: ReservationsFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientConferenceReservations"
    ADD CONSTRAINT "ReservationsFK1" FOREIGN KEY ("ClientID") REFERENCES "Clients"("ClientID") ON DELETE CASCADE;


--
-- TOC entry 1993 (class 2606 OID 30497)
-- Dependencies: 163 169 1954 2133
-- Name: ReservationsFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ClientConferenceReservations"
    ADD CONSTRAINT "ReservationsFK2" FOREIGN KEY ("ConferenceID") REFERENCES "Conference"("ConferenceID") ON DELETE CASCADE;


--
-- TOC entry 2004 (class 2606 OID 22027)
-- Dependencies: 1954 169 177 2133
-- Name: WorkshopFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshops"
    ADD CONSTRAINT "WorkshopFK1" FOREIGN KEY ("ConferenceID") REFERENCES "Conference"("ConferenceID") ON DELETE CASCADE;


--
-- TOC entry 2005 (class 2606 OID 22032)
-- Dependencies: 177 170 1960 2133
-- Name: WorkshopFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "ConferenceWorkshops"
    ADD CONSTRAINT "WorkshopFK2" FOREIGN KEY ("ConferenceDayID") REFERENCES "ConferenceDays"("ConferenceDayID") ON DELETE CASCADE;


--
-- TOC entry 2009 (class 2606 OID 22037)
-- Dependencies: 173 1968 181 2133
-- Name: WorkshopParticipantsFK1; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "WorkshopParticipants"
    ADD CONSTRAINT "WorkshopParticipantsFK1" FOREIGN KEY ("ParticipantID") REFERENCES "ConferenceParticipants"("ParticipantID") ON DELETE CASCADE;


--
-- TOC entry 2010 (class 2606 OID 22042)
-- Dependencies: 181 163 1931 2133
-- Name: WorkshopParticipantsFK2; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "WorkshopParticipants"
    ADD CONSTRAINT "WorkshopParticipantsFK2" FOREIGN KEY ("ReservationID") REFERENCES "ClientConferenceReservations"("ReservationID") ON DELETE CASCADE;


--
-- TOC entry 2011 (class 2606 OID 22047)
-- Dependencies: 177 1981 181 2133
-- Name: WorkshopParticipantsFK3; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "WorkshopParticipants"
    ADD CONSTRAINT "WorkshopParticipantsFK3" FOREIGN KEY ("WorkshopID") REFERENCES "ConferenceWorkshops"("WorkshopID") ON DELETE CASCADE;


--
-- TOC entry 2012 (class 2606 OID 22052)
-- Dependencies: 170 181 1960 2133
-- Name: WorkshopParticipantsFK4; Type: FK CONSTRAINT; Schema: ConferenceManagement; Owner: salceson
--

ALTER TABLE ONLY "WorkshopParticipants"
    ADD CONSTRAINT "WorkshopParticipantsFK4" FOREIGN KEY ("ConferenceDayID") REFERENCES "ConferenceDays"("ConferenceDayID") ON DELETE CASCADE;


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 7
-- Name: ConferenceManagement; Type: ACL; Schema: -; Owner: salceson
--

REVOKE ALL ON SCHEMA "ConferenceManagement" FROM PUBLIC;
REVOKE ALL ON SCHEMA "ConferenceManagement" FROM salceson;
GRANT ALL ON SCHEMA "ConferenceManagement" TO salceson;
GRANT USAGE ON SCHEMA "ConferenceManagement" TO PUBLIC;
GRANT ALL ON SCHEMA "ConferenceManagement" TO conference_management_admin WITH GRANT OPTION;


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 215
-- Name: calculatepayment(integer); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION calculatepayment(reservationid integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION calculatepayment(reservationid integer) FROM salceson;
GRANT ALL ON FUNCTION calculatepayment(reservationid integer) TO salceson;
GRANT ALL ON FUNCTION calculatepayment(reservationid integer) TO PUBLIC;
GRANT ALL ON FUNCTION calculatepayment(reservationid integer) TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION calculatepayment(reservationid integer) TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION calculatepayment(reservationid integer) TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 211
-- Name: conference_days_participants_inserting_trigger(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION conference_days_participants_inserting_trigger() FROM PUBLIC;
REVOKE ALL ON FUNCTION conference_days_participants_inserting_trigger() FROM salceson;
GRANT ALL ON FUNCTION conference_days_participants_inserting_trigger() TO salceson;
GRANT ALL ON FUNCTION conference_days_participants_inserting_trigger() TO PUBLIC;
GRANT ALL ON FUNCTION conference_days_participants_inserting_trigger() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conference_days_participants_inserting_trigger() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conference_days_participants_inserting_trigger() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 208
-- Name: conference_participants_update_trigger(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION conference_participants_update_trigger() FROM PUBLIC;
REVOKE ALL ON FUNCTION conference_participants_update_trigger() FROM salceson;
GRANT ALL ON FUNCTION conference_participants_update_trigger() TO salceson;
GRANT ALL ON FUNCTION conference_participants_update_trigger() TO PUBLIC;
GRANT ALL ON FUNCTION conference_participants_update_trigger() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conference_participants_update_trigger() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conference_participants_update_trigger() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 213
-- Name: conferencedays(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION conferencedays() FROM PUBLIC;
REVOKE ALL ON FUNCTION conferencedays() FROM salceson;
GRANT ALL ON FUNCTION conferencedays() TO salceson;
GRANT ALL ON FUNCTION conferencedays() TO PUBLIC;
GRANT ALL ON FUNCTION conferencedays() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conferencedays() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION conferencedays() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 212
-- Name: discountstrigger(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION discountstrigger() FROM PUBLIC;
REVOKE ALL ON FUNCTION discountstrigger() FROM salceson;
GRANT ALL ON FUNCTION discountstrigger() TO salceson;
GRANT ALL ON FUNCTION discountstrigger() TO PUBLIC;
GRANT ALL ON FUNCTION discountstrigger() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION discountstrigger() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION discountstrigger() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2146 (class 0 OID 0)
-- Dependencies: 206
-- Name: getclientpayments(integer); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION getclientpayments(clientidarg integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION getclientpayments(clientidarg integer) FROM salceson;
GRANT ALL ON FUNCTION getclientpayments(clientidarg integer) TO salceson;
GRANT ALL ON FUNCTION getclientpayments(clientidarg integer) TO PUBLIC;
GRANT ALL ON FUNCTION getclientpayments(clientidarg integer) TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getclientpayments(clientidarg integer) TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getclientpayments(clientidarg integer) TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2147 (class 0 OID 0)
-- Dependencies: 205
-- Name: getconferencedayparticipants(integer); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) FROM salceson;
GRANT ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) TO salceson;
GRANT ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) TO PUBLIC;
GRANT ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getconferencedayparticipants(conferencedayarg integer) TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2148 (class 0 OID 0)
-- Dependencies: 204
-- Name: getworkshopparticipants(integer); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) FROM salceson;
GRANT ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) TO salceson;
GRANT ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) TO PUBLIC;
GRANT ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION getworkshopparticipants(argworkshopid integer) TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 207
-- Name: trigger_insert_days_reservations(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION trigger_insert_days_reservations() FROM PUBLIC;
REVOKE ALL ON FUNCTION trigger_insert_days_reservations() FROM salceson;
GRANT ALL ON FUNCTION trigger_insert_days_reservations() TO salceson;
GRANT ALL ON FUNCTION trigger_insert_days_reservations() TO PUBLIC;
GRANT ALL ON FUNCTION trigger_insert_days_reservations() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_days_reservations() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_days_reservations() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 209
-- Name: trigger_insert_workshops_participants(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION trigger_insert_workshops_participants() FROM PUBLIC;
REVOKE ALL ON FUNCTION trigger_insert_workshops_participants() FROM salceson;
GRANT ALL ON FUNCTION trigger_insert_workshops_participants() TO salceson;
GRANT ALL ON FUNCTION trigger_insert_workshops_participants() TO PUBLIC;
GRANT ALL ON FUNCTION trigger_insert_workshops_participants() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_workshops_participants() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_workshops_participants() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 214
-- Name: trigger_insert_workshops_reservations(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION trigger_insert_workshops_reservations() FROM PUBLIC;
REVOKE ALL ON FUNCTION trigger_insert_workshops_reservations() FROM salceson;
GRANT ALL ON FUNCTION trigger_insert_workshops_reservations() TO salceson;
GRANT ALL ON FUNCTION trigger_insert_workshops_reservations() TO PUBLIC;
GRANT ALL ON FUNCTION trigger_insert_workshops_reservations() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_workshops_reservations() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION trigger_insert_workshops_reservations() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 210
-- Name: workshopstrigger(); Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON FUNCTION workshopstrigger() FROM PUBLIC;
REVOKE ALL ON FUNCTION workshopstrigger() FROM salceson;
GRANT ALL ON FUNCTION workshopstrigger() TO salceson;
GRANT ALL ON FUNCTION workshopstrigger() TO PUBLIC;
GRANT ALL ON FUNCTION workshopstrigger() TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION workshopstrigger() TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT ALL ON FUNCTION workshopstrigger() TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 177
-- Name: ConferenceWorkshops; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceWorkshops" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceWorkshops" FROM salceson;
GRANT ALL ON TABLE "ConferenceWorkshops" TO salceson;
GRANT ALL ON TABLE "ConferenceWorkshops" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceWorkshops" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON TABLE "ConferenceWorkshops" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 181
-- Name: WorkshopParticipants; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "WorkshopParticipants" FROM PUBLIC;
REVOKE ALL ON TABLE "WorkshopParticipants" FROM salceson;
GRANT ALL ON TABLE "WorkshopParticipants" TO salceson;
GRANT ALL ON TABLE "WorkshopParticipants" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "WorkshopParticipants" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "WorkshopParticipants" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 191
-- Name: AttendanceAtWorkshopsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "AttendanceAtWorkshopsView" FROM PUBLIC;
REVOKE ALL ON TABLE "AttendanceAtWorkshopsView" FROM salceson;
GRANT ALL ON TABLE "AttendanceAtWorkshopsView" TO salceson;
GRANT SELECT ON TABLE "AttendanceAtWorkshopsView" TO PUBLIC;


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 162
-- Name: ClientConferenceDaysReservations; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ClientConferenceDaysReservations" FROM PUBLIC;
REVOKE ALL ON TABLE "ClientConferenceDaysReservations" FROM salceson;
GRANT ALL ON TABLE "ClientConferenceDaysReservations" TO salceson;
GRANT ALL ON TABLE "ClientConferenceDaysReservations" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientConferenceDaysReservations" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientConferenceDaysReservations" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 163
-- Name: ClientConferenceReservations; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ClientConferenceReservations" FROM PUBLIC;
REVOKE ALL ON TABLE "ClientConferenceReservations" FROM salceson;
GRANT ALL ON TABLE "ClientConferenceReservations" TO salceson;
GRANT ALL ON TABLE "ClientConferenceReservations" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientConferenceReservations" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientConferenceReservations" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 164
-- Name: ClientConferenceReservations_ReservationID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ClientConferenceReservations_ReservationID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 165
-- Name: ClientPayments; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ClientPayments" FROM PUBLIC;
REVOKE ALL ON TABLE "ClientPayments" FROM salceson;
GRANT ALL ON TABLE "ClientPayments" TO salceson;
GRANT ALL ON TABLE "ClientPayments" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientPayments" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientPayments" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 167
-- Name: Clients; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "Clients" FROM PUBLIC;
REVOKE ALL ON TABLE "Clients" FROM salceson;
GRANT ALL ON TABLE "Clients" TO salceson;
GRANT ALL ON TABLE "Clients" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "Clients" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "Clients" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 175
-- Name: ConferencePaymentsDiscounts; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferencePaymentsDiscounts" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferencePaymentsDiscounts" FROM salceson;
GRANT ALL ON TABLE "ConferencePaymentsDiscounts" TO salceson;
GRANT ALL ON TABLE "ConferencePaymentsDiscounts" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferencePaymentsDiscounts" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferencePaymentsDiscounts" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 185
-- Name: ClientPaymentsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ClientPaymentsView" FROM PUBLIC;
REVOKE ALL ON TABLE "ClientPaymentsView" FROM salceson;
GRANT ALL ON TABLE "ClientPaymentsView" TO salceson;
GRANT ALL ON TABLE "ClientPaymentsView" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientPaymentsView" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ClientPaymentsView" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 166
-- Name: ClientPayments_PaymentID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ClientPayments_PaymentID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ClientPayments_PaymentID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ClientPayments_PaymentID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ClientPayments_PaymentID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ClientPayments_PaymentID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ClientPayments_PaymentID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 168
-- Name: Clients_ClientID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "Clients_ClientID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "Clients_ClientID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "Clients_ClientID_seq" TO salceson;
GRANT ALL ON SEQUENCE "Clients_ClientID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "Clients_ClientID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "Clients_ClientID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2168 (class 0 OID 0)
-- Dependencies: 169
-- Name: Conference; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "Conference" FROM PUBLIC;
REVOKE ALL ON TABLE "Conference" FROM salceson;
GRANT ALL ON TABLE "Conference" TO salceson;
GRANT ALL ON TABLE "Conference" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "Conference" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON TABLE "Conference" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2169 (class 0 OID 0)
-- Dependencies: 170
-- Name: ConferenceDays; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceDays" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceDays" FROM salceson;
GRANT ALL ON TABLE "ConferenceDays" TO salceson;
GRANT ALL ON TABLE "ConferenceDays" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceDays" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON TABLE "ConferenceDays" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2170 (class 0 OID 0)
-- Dependencies: 171
-- Name: ConferenceDaysParticipants; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceDaysParticipants" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceDaysParticipants" FROM salceson;
GRANT ALL ON TABLE "ConferenceDaysParticipants" TO salceson;
GRANT ALL ON TABLE "ConferenceDaysParticipants" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceDaysParticipants" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceDaysParticipants" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2171 (class 0 OID 0)
-- Dependencies: 173
-- Name: ConferenceParticipants; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceParticipants" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceParticipants" FROM salceson;
GRANT ALL ON TABLE "ConferenceParticipants" TO salceson;
GRANT ALL ON TABLE "ConferenceParticipants" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceParticipants" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceParticipants" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2172 (class 0 OID 0)
-- Dependencies: 183
-- Name: ConferenceDaysParticipantsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceDaysParticipantsView" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceDaysParticipantsView" FROM salceson;
GRANT ALL ON TABLE "ConferenceDaysParticipantsView" TO salceson;
GRANT ALL ON TABLE "ConferenceDaysParticipantsView" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceDaysParticipantsView" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceDaysParticipantsView" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2174 (class 0 OID 0)
-- Dependencies: 172
-- Name: ConferenceDays_ConferenceDayID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceDays_ConferenceDayID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2176 (class 0 OID 0)
-- Dependencies: 174
-- Name: ConferenceParticipants_ParticipantID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceParticipants_ParticipantID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 176
-- Name: ConferencePaymentsDiscounts_DiscountID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferencePaymentsDiscounts_DiscountID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 179
-- Name: ConferenceWorkshopsReservations; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "ConferenceWorkshopsReservations" FROM PUBLIC;
REVOKE ALL ON TABLE "ConferenceWorkshopsReservations" FROM salceson;
GRANT ALL ON TABLE "ConferenceWorkshopsReservations" TO salceson;
GRANT ALL ON TABLE "ConferenceWorkshopsReservations" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceWorkshopsReservations" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "ConferenceWorkshopsReservations" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 178
-- Name: ConferenceWorkshops_WorkshopID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" TO salceson;
GRANT ALL ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "ConferenceWorkshops_WorkshopID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 180
-- Name: Conference_ConferenceID_seq; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON SEQUENCE "Conference_ConferenceID_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "Conference_ConferenceID_seq" FROM salceson;
GRANT ALL ON SEQUENCE "Conference_ConferenceID_seq" TO salceson;
GRANT ALL ON SEQUENCE "Conference_ConferenceID_seq" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "Conference_ConferenceID_seq" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT ON SEQUENCE "Conference_ConferenceID_seq" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 189
-- Name: MonthlyEearningReportView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "MonthlyEearningReportView" FROM PUBLIC;
REVOKE ALL ON TABLE "MonthlyEearningReportView" FROM salceson;
GRANT ALL ON TABLE "MonthlyEearningReportView" TO salceson;
GRANT SELECT ON TABLE "MonthlyEearningReportView" TO PUBLIC;


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 187
-- Name: MostEarningConferencesView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "MostEarningConferencesView" FROM PUBLIC;
REVOKE ALL ON TABLE "MostEarningConferencesView" FROM salceson;
GRANT ALL ON TABLE "MostEarningConferencesView" TO salceson;
GRANT SELECT ON TABLE "MostEarningConferencesView" TO PUBLIC;


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 188
-- Name: MostEarningWorkshopsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "MostEarningWorkshopsView" FROM PUBLIC;
REVOKE ALL ON TABLE "MostEarningWorkshopsView" FROM salceson;
GRANT ALL ON TABLE "MostEarningWorkshopsView" TO salceson;
GRANT SELECT ON TABLE "MostEarningWorkshopsView" TO PUBLIC;


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 184
-- Name: MostOftenCustomersView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "MostOftenCustomersView" FROM PUBLIC;
REVOKE ALL ON TABLE "MostOftenCustomersView" FROM salceson;
GRANT ALL ON TABLE "MostOftenCustomersView" TO salceson;
GRANT ALL ON TABLE "MostOftenCustomersView" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "MostOftenCustomersView" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "MostOftenCustomersView" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 186
-- Name: MostPopularWorkshopsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "MostPopularWorkshopsView" FROM PUBLIC;
REVOKE ALL ON TABLE "MostPopularWorkshopsView" FROM salceson;
GRANT ALL ON TABLE "MostPopularWorkshopsView" TO salceson;
GRANT SELECT ON TABLE "MostPopularWorkshopsView" TO PUBLIC;


--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 190
-- Name: PaymentsNotPaidOnTimeView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "PaymentsNotPaidOnTimeView" FROM PUBLIC;
REVOKE ALL ON TABLE "PaymentsNotPaidOnTimeView" FROM salceson;
GRANT ALL ON TABLE "PaymentsNotPaidOnTimeView" TO salceson;
GRANT SELECT ON TABLE "PaymentsNotPaidOnTimeView" TO PUBLIC;


--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 182
-- Name: WorkshopParticipantsView; Type: ACL; Schema: ConferenceManagement; Owner: salceson
--

REVOKE ALL ON TABLE "WorkshopParticipantsView" FROM PUBLIC;
REVOKE ALL ON TABLE "WorkshopParticipantsView" FROM salceson;
GRANT ALL ON TABLE "WorkshopParticipantsView" TO salceson;
GRANT ALL ON TABLE "WorkshopParticipantsView" TO conference_management_admin WITH GRANT OPTION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "WorkshopParticipantsView" TO conference_management_worker;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION conference_management_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "WorkshopParticipantsView" TO conference_management_client;
RESET SESSION AUTHORIZATION;


--
-- TOC entry 1531 (class 826 OID 30685)
-- Dependencies: 7 2133
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: ConferenceManagement; Owner: salceson
--

ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON SEQUENCES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON SEQUENCES  FROM salceson;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" GRANT SELECT ON SEQUENCES  TO PUBLIC;


--
-- TOC entry 1532 (class 826 OID 30686)
-- Dependencies: 7 2133
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: ConferenceManagement; Owner: salceson
--

ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON FUNCTIONS  FROM salceson;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" GRANT ALL ON FUNCTIONS  TO PUBLIC;


--
-- TOC entry 1529 (class 826 OID 30683)
-- Dependencies: 2133
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: salceson
--

ALTER DEFAULT PRIVILEGES FOR ROLE salceson REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson REVOKE ALL ON TABLES  FROM salceson;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson GRANT ALL ON TABLES  TO salceson;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson GRANT SELECT ON TABLES  TO PUBLIC;


--
-- TOC entry 1530 (class 826 OID 30684)
-- Dependencies: 7 2133
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: ConferenceManagement; Owner: salceson
--

ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" REVOKE ALL ON TABLES  FROM salceson;
ALTER DEFAULT PRIVILEGES FOR ROLE salceson IN SCHEMA "ConferenceManagement" GRANT SELECT ON TABLES  TO PUBLIC;


-- Completed on 2014-01-21 12:17:16 CET

--
-- PostgreSQL database dump complete
--

