package pl.edu.agh.ki.conferencemanagement.generators;

import pl.edu.agh.ki.conferencemanagement.domain.Conference;

import java.sql.Date;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceDataGenerator extends AbstractDataGenerator<Conference> {

    public static final String[] names = {"Java Overview", "Spring Overview", "Spring Security & MVC",
            "Hibernate", "Clean code", "Android", "Enterprise projects", "Test Driven Development"};
    private static final String[] descriptions = {
            "We show the basics of Java",
            "We show the basics of Spring including databases connection",
            "We show the basics of web development in Spring",
            "We show the basics of Hibernate data persistance",
            "We show the basics of keeping your code clean",
            "We show how to develop useful apps for Android",
            "We show the basics of Java Enterprise development",
            "We show how to turn to TDD in your company"};
    private Date startDate;

    @Override
    public Conference generate() {
        Conference conference = new Conference();
        String endDateStart = startDate.toString().substring(0,8);
        int day = Integer.parseInt(startDate.toString().substring(8, 10));
        day += 2+random.nextInt(2);
        Date endDate = Date.valueOf(endDateStart+day);
        conference.setStartDate(startDate);
        conference.setEndDate(endDate);
        int i = random.nextInt(names.length);
        conference.setConferenceName(names[i]);
        conference.setDescription(descriptions[i]);
        return conference;
    }

    public void setStartDate(Date date){
        startDate=date;
    }
}
