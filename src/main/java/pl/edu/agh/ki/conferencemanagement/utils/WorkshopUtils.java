package pl.edu.agh.ki.conferencemanagement.utils;

import java.util.*;

/**
 * @author Michał Ciołczyk
 */
public class WorkshopUtils {
    private static Map<String, List<String>> workshops;
    private static Random random = new Random();

    public static String getName(String conferenceName){
        if(workshops == null){
            setupMap();
        }

        if(!workshops.containsKey(conferenceName)){
            return null;
        }

        List<String> workshopNames = workshops.get(conferenceName);
        return workshopNames.get(random.nextInt(workshopNames.size()));
    }

    private static void setupMap() {
        workshops =  new HashMap<String, List<String>>();
        workshops.put("Java Overview", Arrays.asList(
                "Introduction to OOP",
                "First steps",
                "Collections",
                "I/O",
                "Design Patterns",
                "Design Patterns II",
                "Annotations",
                "Maven"
        ));
        workshops.put("Spring Overview", Arrays.asList(
                "Introduction to Spring - beans configuration",
                "Dependency Injection",
                "Aspect-Oriented Programming",
                "Spring Annotations",
                "Spring in Enterprise Applications"
        ));
        workshops.put("Hibernate", Arrays.asList(
                "Relative databases",
                "Hibernate configuration",
                "Hibernate annotations",
                "Hibernate with Spring",
                "Hibernate with generic DAO",
                "Hibernate in Enterprise Applications"
        ));
        workshops.put("Spring Security & MVC", Arrays.asList(
                "Spring Security configuration",
                "Spring MVC configuration",
                "Creating views with JSP",
                "Advanced controllers with Spring",
                "Authorization in Spring MVC",
                "Creating simple CMS in Spring MVC",
                "Web development with Spring (Enterprise)"
        ));
        workshops.put("Clean code", Arrays.asList(
                "Variables naming",
                "Function naming",
                "Classes naming",
                "Clean functions",
                "Clean classes",
                "Clean tests",
                "Formatting code"
        ));
        workshops.put("Android", Arrays.asList(
                "First Android application",
                "Android App Lifecycle",
                "Android UI Components",
                "Android UI Advanced Components",
                "Android - how to write fast code?"
        ));
        workshops.put("Enterprise projects", Arrays.asList(
                "Java Beans",
                "SVN",
                "Designing",
                "Agile",
                "Scrum",
                "Java Frameworks overview"
        ));
        workshops.put("Test Driven Development", Arrays.asList(
                "TDD - how to start?",
                "JUnit & Mockito",
                "Writing good tests",
                "Integrating tests",
                "Unit tests",
                "Integration Code Servers"
        ));
    }
}
