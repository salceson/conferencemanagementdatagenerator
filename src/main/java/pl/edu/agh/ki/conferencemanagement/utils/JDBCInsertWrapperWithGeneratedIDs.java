package pl.edu.agh.ki.conferencemanagement.utils;

import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import pl.edu.agh.ki.conferencemanagement.domain.Entity;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author Michał Ciołczyk
 */
public class JDBCInsertWrapperWithGeneratedIDs implements JDBCInsertWrapper {
    private DataSource dataSource;
    private String schemaName;
    private SimpleJdbcInsert simpleJdbcInsert;

    @Override
    public void setupConnection(DataSource dataSource, String schemaName) {
        this.dataSource=dataSource;
        this.schemaName=schemaName;
    }

    @Override
    public void insert(Entity entity) {
        simpleJdbcInsert = new SimpleJdbcInsert(dataSource);
        simpleJdbcInsert.withSchemaName(schemaName);
        simpleJdbcInsert.withTableName(entity.getTableName());
        simpleJdbcInsert.usingGeneratedKeyColumns(entity.getIDColumnName());
        Map<String,Object> objectMap = entity.getObjectsForInsertOperation();
        Object[] columnObjects = objectMap.keySet().toArray();
        String[] columns = new String[columnObjects.length];
        int i=0;
        for (Object columnObject : columnObjects) {
            columns[i]=(String)columnObjects[i];
            i++;
        }
        simpleJdbcInsert.usingColumns(columns);
        Number number = simpleJdbcInsert.executeAndReturnKey(objectMap);
        entity.setID(number.intValue());
    }
}
