package pl.edu.agh.ki.conferencemanagement.domain;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michał Ciołczyk
 */
public class Conference implements Entity {
    private Integer conferenceID;
    private String conferenceName;
    private Date startDate;
    private Date endDate;
    private String description;

    public Conference() {}

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("\"ConferenceName\"", conferenceName);
        map.put("\"ConferenceStartDate\"", startDate);
        map.put("\"ConferenceEndDate\"", endDate);
        map.put("\"ConferenceDescription\"", description);

        return map;
    }

    @Override
    public String getTableName() {
        return "\"Conference\"";
    }

    @Override
    public void setID(Integer id) {
        conferenceID=id;
    }

    @Override
    public String getIDColumnName() {
        return "ConferenceID";
    }

    public Integer getId() {
        return conferenceID;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
