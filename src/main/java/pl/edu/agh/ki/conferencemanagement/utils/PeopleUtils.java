package pl.edu.agh.ki.conferencemanagement.utils;

import java.util.List;
import java.util.Random;

/**
 * @author Michał Ciołczyk
 */
public class PeopleUtils {
    private static PeopleUtils instance;
    private List<String> firstNames;
    private List<String> lastNames;
    private List<String> cities;
    private List<String> streets;
    private Random random;

    private PeopleUtils(){
        random = new Random();
        final String path = "src/main/resources/data/";
        firstNames = FileLoader.loadListFromTextFile(path+"imiona.txt");
        lastNames = FileLoader.loadListFromTextFile(path+"nazwiska.txt");
        cities = FileLoader.loadListFromTextFile(path+"miasta.txt");
        streets = FileLoader.loadListFromTextFile(path+"ulice.txt");
    }

    public static PeopleUtils getInstance(){
        if(instance==null){
            instance = new PeopleUtils();
        }
        return instance;
    }

    public String getName(){
        return getFirstName() + " " + getLastName();
    }

    public String getFirstName() {
        return getFromList(firstNames);
    }

    private String getFromList(List<String> list) {
        return list.get(random.nextInt(list.size()));
    }

    public String getLastName() {
        return getFromList(lastNames);
    }

    public String getCity(){
        return getFromList(cities);
    }

    public String getStreet(){
        return getFromList(streets);
    }
}
