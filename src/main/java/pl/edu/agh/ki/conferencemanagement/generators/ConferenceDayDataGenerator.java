package pl.edu.agh.ki.conferencemanagement.generators;

import pl.edu.agh.ki.conferencemanagement.domain.Conference;
import pl.edu.agh.ki.conferencemanagement.domain.ConferenceDay;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceDayDataGenerator extends AbstractDataGenerator<ConferenceDay> {
    private Conference conference;
    private int day;

    @Override
    public ConferenceDay generate() {
        ConferenceDay conferenceDay = new ConferenceDay();

        conferenceDay.setConferenceId(conference.getId());
        conferenceDay.setDay(day);
        String startDateString = conference.getStartDate().toString();
        int dateDay = Integer.parseInt(startDateString.substring(8,10))+day;
        String dateString = startDateString.substring(0, 8)+((dateDay<10)?"0":"")+dateDay;
        conferenceDay.setDate(Date.valueOf(dateString));
        conferenceDay.setCapacity((random.nextInt(2)>0)?75:150);
        conferenceDay.setFee(BigDecimal.valueOf((long)(random.nextInt(75000)+1),2));
        conferenceDay.setStudentDiscount(1.0-random.nextDouble()*0.2);

        return conferenceDay;
    }

    public void setConference(Conference conference){
        this.conference = conference;
    }

    public void setDay(int day){
        this.day=day;
    }
}
