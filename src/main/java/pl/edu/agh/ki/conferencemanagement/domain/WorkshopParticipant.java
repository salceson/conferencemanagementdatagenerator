package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:18
 */
public class WorkshopParticipant implements Entity {
    private Integer participantID;
    private Integer reservationID;
    private Integer workshopID;
    private Integer conferenceDayID;

    public Integer getParticipantID() {
        return participantID;
    }

    public void setParticipantID(Integer participantID) {
        this.participantID = participantID;
    }

    public Integer getReservationID() {
        return reservationID;
    }

    public void setReservationID(Integer reservationID) {
        this.reservationID = reservationID;
    }

    public Integer getWorkshopID() {
        return workshopID;
    }

    public void setWorkshopID(Integer workshopID) {
        this.workshopID = workshopID;
    }

    public Integer getConferenceDayID() {
        return conferenceDayID;
    }

    public void setConferenceDayID(Integer conferenceDayID) {
        this.conferenceDayID = conferenceDayID;
    }

    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("\"ParticipantID\"", participantID);
        map.put("\"ReservationID\"", reservationID);
        map.put("\"WorkshopID\"", workshopID);
        map.put("\"ConferenceDayID\"", conferenceDayID);

        return map;
    }

    @Override
    public String getTableName() {
        return "\"WorkshopParticipants\"";
    }

    @Override
    public void setID(Integer id) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getIDColumnName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
