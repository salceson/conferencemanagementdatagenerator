package pl.edu.agh.ki.conferencemanagement.domain;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceDay implements Entity{
    private Integer id;
    private Integer conferenceId;
    private Integer capacity;
    private Date date;
    private BigDecimal fee;
    private double studentDiscount;
    private Integer day;


    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("\"ConferenceID\"", conferenceId);
        map.put("\"ConferenceDay\"", day);
        map.put("\"ConferenceDayDate\"", date);
        map.put("\"ConferenceDayFee\"", fee);
        map.put("\"ConferenceDayStudentDiscount\"", studentDiscount);
        map.put("\"ConferenceDayCapacity\"", capacity);

        return map;
    }

    @Override
    public String getTableName() {
        return "\"ConferenceDays\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "ConferenceDayID";
    }

    public Integer getId() {
        return id;
    }

    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public double getStudentDiscount() {
        return studentDiscount;
    }

    public void setStudentDiscount(double studentDiscount) {
        this.studentDiscount = studentDiscount;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
