package pl.edu.agh.ki.conferencemanagement.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author Michał Ciołczyk
 */
public class SetUtils {
    public static<T> Set<T> filter(Set<T> set, Predicate<T> predicate){
        Class clazz = set.getClass();
        Set<T> newSet;
        try {
            newSet = (Set<T>) clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            return Collections.emptySet();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return Collections.emptySet();
        }

        for (T object : set) {
            if(predicate.apply(object)){
                newSet.add(object);
            }
        }
        return newSet;
    }
}
