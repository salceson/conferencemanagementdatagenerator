package pl.edu.agh.ki.conferencemanagement.generators;

import java.util.Random;

/**
 * @author Michał Ciołczyk
 */
public abstract class AbstractDataGenerator<T> implements DataGenerator<T> {
    protected final Random random = new Random();

    @Override
    public abstract T generate();

}
