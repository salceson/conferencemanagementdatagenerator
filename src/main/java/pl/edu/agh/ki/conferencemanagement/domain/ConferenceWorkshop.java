package pl.edu.agh.ki.conferencemanagement.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:45
 */
public class ConferenceWorkshop implements Entity {
    private Integer id;
    private Integer conferenceId;
    private Integer conferenceDayId;
    private Timestamp workshopStartDate;
    private Timestamp workshopEndDate;
    private Integer workshopCapacity;
    private String workshopConductor;
    private String workshopName;
    private BigDecimal workshopFee;
    private double workshopStudentDiscount;

    public Integer getId() {
        return id;
    }

    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }

    public Integer getConferenceDayId() {
        return conferenceDayId;
    }

    public void setConferenceDayId(Integer conferenceDayId) {
        this.conferenceDayId = conferenceDayId;
    }

    public Timestamp getWorkshopStartDate() {
        return workshopStartDate;
    }

    public void setWorkshopStartDate(Timestamp workshopStartDate) {
        this.workshopStartDate = workshopStartDate;
    }

    public Timestamp getWorkshopEndDate() {
        return workshopEndDate;
    }

    public void setWorkshopEndDate(Timestamp workshopEndDate) {
        this.workshopEndDate = workshopEndDate;
    }

    public Integer getWorkshopCapacity() {
        return workshopCapacity;
    }

    public void setWorkshopCapacity(Integer workshopCapacity) {
        this.workshopCapacity = workshopCapacity;
    }

    public String getWorkshopConductor() {
        return workshopConductor;
    }

    public void setWorkshopConductor(String workshopConductor) {
        this.workshopConductor = workshopConductor;
    }

    public String getWorkshopName() {
        return workshopName;
    }

    public void setWorkshopName(String workshopName) {
        this.workshopName = workshopName;
    }

    public BigDecimal getWorkshopFee() {
        return workshopFee;
    }

    public void setWorkshopFee(BigDecimal workshopFee) {
        this.workshopFee = workshopFee;
    }

    public double getWorkshopStudentDiscount() {
        return workshopStudentDiscount;
    }

    public void setWorkshopStudentDiscount(double workshopStudentDiscount) {
        this.workshopStudentDiscount = workshopStudentDiscount;
    }

    @Override
    public Map<String,Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"ConferenceID\"", conferenceId);
        objects.put("\"ConferenceDayID\"", conferenceDayId);
        objects.put("\"WorkshopStartDate\"", workshopStartDate);
        objects.put("\"WorkshopEndDate\"", workshopEndDate);
        objects.put("\"WorkshopCapacity\"", workshopCapacity);
        objects.put("\"WorkshopConductor\"", workshopConductor);
        objects.put("\"WorkshopName\"", workshopName);
        objects.put("\"WorkshopFee\"", workshopFee);
        objects.put("\"WorkshopStudentDiscount\"", workshopStudentDiscount);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ConferenceWorkshops\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "WorkshopID";
    }
}
