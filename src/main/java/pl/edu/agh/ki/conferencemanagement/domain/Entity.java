package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.Map;

/**
 * @author Michał Ciołczyk
 */
public interface Entity {
    Map<String,Object> getObjectsForInsertOperation();
    String getTableName();
    void setID(Integer id);
    String getIDColumnName();
}
