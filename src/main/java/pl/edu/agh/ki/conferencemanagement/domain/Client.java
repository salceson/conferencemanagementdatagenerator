package pl.edu.agh.ki.conferencemanagement.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michał Ciołczyk
 */
public class Client implements Entity {
    private Integer id;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String country;
    private String type;
    private Timestamp registrationDate;
    private String companyName;
    private String regon;
    private String nip;
    private String phone;
    private String username;
    private String password;

    public Client() {}

    @Override
    public String toString(){
        return id+": "+type+": "+firstName+" "+lastName+"; "+country+", "+city+", "+address;
    }


    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Timestamp getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Timestamp registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Map<String,Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"ClientType\"", type);
        objects.put("\"ClientFirstName\"", firstName);
        objects.put("\"ClientLastName\"", lastName);
        objects.put("\"ClientCompanyName\"", companyName);
        objects.put("\"ClientNIP\"", nip);
        objects.put("\"ClientREGON\"", regon);
        objects.put("\"ClientAddress\"", address);
        objects.put("\"ClientCity\"", city);
        objects.put("\"ClientCountry\"", country);
        objects.put("\"ClientRegistrationDate\"", registrationDate);
        objects.put("\"ClientPhone\"", phone);
        objects.put("\"ClientUsername\"", username);
        objects.put("\"ClientPassword\"", password);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"Clients\"";
    }

    @Override
    public void setID(Integer id) {
        this.id=id;
    }

    @Override
    public String getIDColumnName() {
        return "ClientID";
    }
}
