package pl.edu.agh.ki.conferencemanagement.generators;

import pl.edu.agh.ki.conferencemanagement.domain.Conference;
import pl.edu.agh.ki.conferencemanagement.domain.ConferenceDay;
import pl.edu.agh.ki.conferencemanagement.domain.ConferenceWorkshop;
import pl.edu.agh.ki.conferencemanagement.utils.PeopleUtils;
import pl.edu.agh.ki.conferencemanagement.utils.WorkshopUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceWorkshopGenerator extends AbstractDataGenerator<ConferenceWorkshop> {
    private ConferenceDay conferenceDay;
    private Conference conference;
    private final PeopleUtils peopleUtils;
    private int startHour;

    public ConferenceWorkshopGenerator() {
        super();
        peopleUtils = PeopleUtils.getInstance();
    }

    @Override
    public ConferenceWorkshop generate() {
        ConferenceWorkshop workshop = new ConferenceWorkshop();

        workshop.setConferenceId(conference.getId());
        workshop.setConferenceDayId(conferenceDay.getId());
        workshop.setWorkshopConductor(peopleUtils.getName());
        workshop.setWorkshopName(WorkshopUtils.getName(conference.getConferenceName()));
        workshop.setWorkshopFee(BigDecimal.valueOf((long)(random.nextInt(75000)*random.nextInt(2)),2));
        workshop.setWorkshopStudentDiscount((workshop.getWorkshopFee().equals(0))?1:random.nextDouble()*0.3);
        workshop.setWorkshopCapacity(random.nextInt(2)==0?40:60);
        String startTime = conferenceDay.getDate().toString()+" "+(startHour<10?"0":"")
                +startHour+":00:00";
        int endHour = startHour+2;
        String endTime = conferenceDay.getDate().toString()+" "+(endHour<10?"0":"")
                +endHour+":00:00";
        workshop.setWorkshopStartDate(Timestamp.valueOf(startTime));
        workshop.setWorkshopEndDate(Timestamp.valueOf(endTime));

        return workshop;
    }

    public void setConference(Conference conference) {
        this.conference = conference;
    }

    public void setConferenceDay(ConferenceDay conferenceDay) {
        this.conferenceDay = conferenceDay;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }
}
