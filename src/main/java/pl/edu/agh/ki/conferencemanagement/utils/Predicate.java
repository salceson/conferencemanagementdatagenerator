package pl.edu.agh.ki.conferencemanagement.utils;

/**
 * @author Michał Ciołczyk
 */
public interface Predicate<T> {
    boolean apply(T object);
}
