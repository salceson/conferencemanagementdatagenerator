package pl.edu.agh.ki.conferencemanagement.generators;

import pl.edu.agh.ki.conferencemanagement.domain.Client;
import pl.edu.agh.ki.conferencemanagement.domain.ConferenceParticipant;
import pl.edu.agh.ki.conferencemanagement.domain.ConferenceReservation;
import pl.edu.agh.ki.conferencemanagement.utils.PeopleUtils;

/**
 * @author Michał Ciołczyk
 */
public class ConferenceParticipantGenerator extends AbstractDataGenerator<ConferenceParticipant> {
    private Client client;
    private final PeopleUtils peopleUtils = PeopleUtils.getInstance();
    private ConferenceReservation reservation;

    @Override
    public ConferenceParticipant generate() {
        ConferenceParticipant participant = new ConferenceParticipant();
        participant.setClientId(client.getId());
        participant.setParticipantFirstName(peopleUtils.getFirstName());
        participant.setParticipantLastName(peopleUtils.getLastName());
        String studentIDCard = null;
        if(random.nextInt(2)>0){
            studentIDCard = Long.toString((long) random.nextInt(899999)+100000L);
        }
        participant.setStudentIDCard(studentIDCard);
        participant.setReservationId(reservation.getId());

        return participant;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setReservation(ConferenceReservation reservation) {
        this.reservation = reservation;
    }
}
