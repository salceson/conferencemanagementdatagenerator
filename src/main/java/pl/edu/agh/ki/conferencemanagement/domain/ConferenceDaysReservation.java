package pl.edu.agh.ki.conferencemanagement.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 01:08
 */
public class ConferenceDaysReservation implements Entity {
    private Integer reservationId;
    private Integer conferenceDayId;
    private Integer numOfPeople;
    private Integer numOfStudents;

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getConferenceDayId() {
        return conferenceDayId;
    }

    public void setConferenceDayId(Integer conferenceDayId) {
        this.conferenceDayId = conferenceDayId;
    }

    public Integer getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(Integer numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public Integer getNumOfStudents() {
        return numOfStudents;
    }

    public void setNumOfStudents(Integer numOfStudents) {
        this.numOfStudents = numOfStudents;
    }





    @Override
    public Map<String, Object> getObjectsForInsertOperation() {
        Map<String,Object> objects=new HashMap<String,Object>();

        objects.put("\"ReservationID\"", reservationId);
        objects.put("\"ConferenceDayID\"", conferenceDayId);
        objects.put("\"NumOfPeople\"", numOfPeople);
        objects.put("\"NumOfStudents\"", numOfStudents);

        return objects;
    }

    @Override
    public String getTableName() {
        return "\"ClientConferenceDaysReservations\"";
    }

    @Override
    public void setID(Integer id) {

    }

    @Override
    public String getIDColumnName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
