package pl.edu.agh.ki.conferencemanagement.generators;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;
import pl.edu.agh.ki.conferencemanagement.domain.Client;

/**
 * Created with IntelliJ IDEA.
 * User: Michał Janczykowski
 * Date: 08.01.14
 * Time: 02:37
 */
public class ClientDataGeneratorTest {
    private ClientDataGenerator instance;

    @Before
    public void setUp() throws Exception {
        instance = new ClientDataGenerator();
    }

    @Test
    public void testGenerate() throws Exception {
        Client client = instance.generate();

        System.out.println(client.toString());

        Assert.isTrue(true);
    }
}
